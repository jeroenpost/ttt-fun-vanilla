ENT.Type = "anim"
ENT.Base = "base_follower"
ENT.RenderGroup = RENDERGROUP_TRANSLUCENT
ENT.AutomaticFrameAdvance = true 

function ENT:Initialize()
	
	self.ModelString = 'models/Batman/slow/jamis/mkvsdcu/batman/slow_pub_v2.mdl'
	self.ModelScale = 0.25
	self.Particles = "feathers_black"




    self.BaseClass.Initialize( self )


	
	if CLIENT then
		self:Fo_AttachParticles( "dark_clouds" )
	end
	
	if SERVER then
		self:Fo_AddRandomSound( "npc/crow/idle3.wav", 100, 100 )
		self:Fo_AddRandomSound( "npc/crow/idle4.wav", 100, 100 )
		self:Fo_AddRandomSound( "npc/crow/pain1.wav", 100, 100 )
	end

end

function ENT:Fo_UpdatePet( speed, weight )
	
	local z = self:GetVelocity().z
	local sequence = self:LookupSequence( "ACT_HL2MP_IDLE_FIST" )
	if ( weight >= 1 ) and z < 0 then
		sequence = self:LookupSequence( "ACT_HL2MP_RUN_SMG1" )
        self:NextThink( CurTime() + 2 )
	end
	
	self:SetPlaybackRate( 1 )
     self:ResetSequence(sequence)
end