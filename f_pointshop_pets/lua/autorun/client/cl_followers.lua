local Player = FindMetaTable( "Player" )
local Entity = FindMetaTable( "Entity" )

function Entity:Fo_AttachParticles( effectName )
	
	if ( not effectName ) or ( not fo.EnableAllParticles ) then return end
	if ( not fo.ShowFirstPersonParticles ) and ( self:Fo_GetOwner() == LocalPlayer() ) then return end
	
	local vPoint = self:GetPos()
	local effectdata = EffectData()
	effectdata:SetStart( vPoint ) // not sure if ( we need a start and origin ( endpoint ) for this effect, but whatever
	effectdata:SetOrigin( vPoint )
	effectdata:SetEntity( self )
	effectdata:SetScale( 1 )
	util.Effect( effectName, effectdata )

end

function Entity:Fo_Draw()
	if ( fo.ShowFirstPersonFollower ) then self:DrawModel()	end
end

// Third preson controlling test.
hook.Add("CalcView", "Fo_CalcView", function( ply, pos, angles, fov )
	
	if ( ply:Fo_ShouldSpec() ) then
	
		local view = {}
		view.origin = pos
		view.angles = angles
		view.fov = fov
		
		local pet = ply:Fo_GetSpecEntity()
		pos = pet:GetPos() + fo.ViewOffset;
		targetPos = pos + angles:Forward() * -80;
		
		local tracedata = {}
		tracedata.start = pos
		tracedata.endpos = targetPos
		tracedata.filter = { pet }
		
		local trace = util.TraceLine(tracedata)
		if trace.Hit then
		   targetPos = trace.HitPos
		end
		
		view.origin = targetPos
		
		return view
	
	end

end )
 
hook.Add("ShouldDrawLocalPlayer", "Fo_ShouldDrawLocalPlayer", function(ply)
        return ply:Fo_ShouldSpec()
end)


// Remove bindings.
hook.Add( "PlayerBindPress", "Fo_PlayerBindPress", function( ply, bind )
	if ( ply:Fo_ShouldSpec() ) and (( bind == "+attack" ) or ( bind == "+duck" ) or ( bind == "+jump" ) ) then return true end
end )