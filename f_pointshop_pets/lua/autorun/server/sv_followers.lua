local Player = FindMetaTable( "Player" )
local Entity = FindMetaTable( "Entity" )

function Player:Fo_AttachParticle( model, effectName )
	
	if ( model.Effect ) then return end
	
	model.Effect = true 
	self.followEnt = model
	local vPoint = self:GetPos()
	local effectdata = EffectData()
	effectdata:SetStart( vPoint ) // not sure if ( we need a start and origin ( endpoint ) for this effect, but whatever
	effectdata:SetOrigin( vPoint )
	effectdata:SetEntity( self )
	effectdata:SetScale( 1 )
	util.Effect( effectName, effectdata )

end

// Get the player's follower list.
function Player:Fo_GetFollowers()
	return self.Fo_Followers;
end

// Get a single follower
function Player:Fo_GetFollower( entName )
	if ( self.Fo_Followers ) then
		return self.Fo_Followers[entName]
	else
		return nil
	end
end

function Player:Fo_AddFollower( ent )
	if ( not IsValid( self ) ) or ( not IsValid( ent ) ) then return end
	self.Fo_Followers[ent:GetClass()] = ent;
end

// Create a follower with this entity name.
function Player:Fo_CreateFollower( entName )
	
	if ( not self.Fo_Followers ) then self.Fo_Followers = {} end
	if ( IsValid( self.Fo_Followers[entName] ) ) then SafeRemoveEntity( self.Fo_Followers[entName] ) end
	
	local ent = ents.Create( entName )
	ent:Fo_SetOwner( self )
	ent:SetPos( self:GetPos() + Vector( 0, 0, 60 ) )
	ent:Spawn()
	
	self:Fo_AddFollower( ent )
	
	return ent
	
end

function Player:Fo_RemoveFollower( entName )
	if ( not IsValid( self ) ) or ( not IsValid( self:Fo_GetFollower( entName ) ) ) then return end
	
	local ent = self:Fo_GetFollower( entName )
	if ( IsValid( ent ) ) then
		SafeRemoveEntity( ent )
	end
	
	if ( self.Fo_Followers ) and ( self.Fo_Followers[entName] ) then
		self.Fo_Followers[entName] = nil;
	end
end

// Remove all followers
function Fo_RemoveAllFollowers()
	
	for _,ply in pairs( player.GetAll() ) do
		for entName,ent in pairs( ply:Fo_GetFollowers() ) do
			ply:Fo_RemoveFollower( entName )
		end
	end
	
end