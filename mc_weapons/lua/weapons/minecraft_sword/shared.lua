

if SERVER then
AddCSLuaFile( "shared.lua" )

end

SWEP.EquipMenuData = {
      type  = "item_weapon",
      name  = "Minecraft Sword",
      desc  = "Mine some heads with this beauty"
      }

SWEP.Contact 		= "ishotz@chillville.net"
SWEP.Author			= "iShotz"
SWEP.Instructions	= "Left Click to attack"
SWEP.ViewModelFlip		= false

SWEP.HoldType			= "melee"
SWEP.Kind = WEAPON_MELEE

SWEP.CanBuy = {ROLE_TRAITOR,ROLE_DETECTIVE}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable		= false
SWEP.ViewModelFOV 	= 70
SWEP.ViewModel			= "models/weapons/v_diamond_gb_sword.mdl"
SWEP.WorldModel			= "models/weapons/w_diamond_gb_sword.mdl"
SWEP.HoldType = "crowbar"
SWEP.Icon = "vgui/ttt_fun_killicons/minecraftsword.png"

SWEP.FiresUnderwater = true
SWEP.Primary.Damage         = 50
SWEP.Base					= "gb_base"
SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"
SWEP.Primary.Delay = 0.5


SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Category			= "Minecraft"
SWEP.PrintName			= "Minecraft Sword"
SWEP.Slot				= 0
SWEP.SlotPos			= 1
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true




function SWEP:Initialize()
self:SetHoldType( "melee" )
end

function SWEP:PrimaryAttack()

   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )




   local spos = self.Owner:GetShootPos()
   local sdest = spos + (self.Owner:GetAimVector() * 90)

   local kmins = Vector(1,1,1) * -10
   local kmaxs = Vector(1,1,1) * 10

   local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

   -- Hull might hit environment stuff that line does not hit
   if not IsValid(tr.Entity) then
      tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
   end

   local hitEnt = tr.Entity

   -- effects
   if IsValid(hitEnt) then
      self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
	  

      local edata = EffectData()
      edata:SetStart(spos)
      edata:SetOrigin(tr.HitPos)
      edata:SetNormal(tr.Normal)
      edata:SetEntity(hitEnt)

      if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
         util.Effect("BloodImpact", edata)
      end
   else
      self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
   end

   if SERVER then
      self.Owner:SetAnimation( PLAYER_ATTACK1 )
   end
self.Weapon:SetNextPrimaryFire(CurTime() + .43)

local trace = self.Owner:GetEyeTrace()

if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 75 then


        local dmg = DamageInfo()
         dmg:SetDamage(self.Primary.Damage)
         dmg:SetAttacker(self.Owner)
         dmg:SetInflictor(self.Weapon)
         dmg:SetDamageForce(self.Owner:GetAimVector() * 1500)
         dmg:SetDamagePosition(self.Owner:GetPos())
         dmg:SetDamageType(DMG_CLUB)
            if not IsValid(hitEnt) then return end
         hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)


	--bullet = {}
	--bullet.Num    = 1
	--bullet.Src    = self.Owner:GetShootPos()
	--bullet.Dir    = self.Owner:GetAimVector()
	--bullet.Spread = Vector(0, 0, 0)
	--bullet.Tracer = 0
	--bullet.Force  = 3
	--bullet.Damage = 60
       --  dmg:SetDamageType(DMG_CLUB)
		self.Owner:DoAttackEvent()
--self.Owner:FireBullets(bullet) 
self.Weapon:EmitSound("Weapon_Crowbar.Melee_Hit")
else
self.Weapon:EmitSound("Zombie.AttackMiss")

	self.Owner:DoAttackEvent()
end

end
 
