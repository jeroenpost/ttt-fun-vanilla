------------------------------ MapVote ------------------------------
function ulx.startmapvote( calling_ply )

    gb_mapvote.AdminStartVote()

    ulx.fancyLogAdmin( calling_ply, "#A Started a mapvote", command )
end
local startmapvote = ulx.command( "TTT Fun", "ulx startmapvote", ulx.startmapvote, "!startmapvote" )
startmapvote:defaultAccess( ULib.ACCESS_ADMIN )
startmapvote:help( "Start a Mapvote" )