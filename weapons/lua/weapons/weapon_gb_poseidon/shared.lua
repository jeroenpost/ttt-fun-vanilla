

if SERVER then
   AddCSLuaFile( "shared.lua" )
   
   --resource.AddFile("materials/vgui/ttt_fun_killicons/poseidons_fork.png")
    --resource.AddFile("sound/weapons/gb_weapons/poseidon_bubble.mp3")





end

SWEP.HoldType           = "pistol"
   SWEP.PrintName          = "Poseidons Trident"

   SWEP.Slot               = 7



if CLIENT then

         SWEP.EquipMenuData = {
      type="Weapon",
      desc="Drowns a player and turns them into a fish"
   };
end
SWEP.AutoSpawnable      = false
--SWEP.CanBuy = {ROLE_TRAITOR} -- only traitors can buy
SWEP.LimitedStock = true -- only buyable once

SWEP.Base               = "gb_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_EQUIP
SWEP.WeaponID = AMMO_RIFLE

SWEP.Primary.Delay          = 1.5
SWEP.Primary.Recoil         = 1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = nil
SWEP.Primary.Damage = 0
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 2
SWEP.Primary.ClipMax = 2-- keep mirrored to ammo
SWEP.Primary.DefaultClip = 2
SWEP.Icon               = "vgui/ttt_fun_killicons/poseidons_fork.png"
SWEP.HeadshotMultiplier = 4
SWEP.Primary.Sound = Sound("weapons/gb_weapons/poseidon_bubble.mp3")

SWEP.AmmoEnt = "nil"

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 21
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/c_superphyscannon.mdl"

SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBoneMods = {}

SWEP.VElements = {
	["element_name"] = { type = "Model", model = "models/props/cs_militia/river02.mdl", bone = "ValveBiped.Bip01", rel = "", pos = Vector(-6.818, -65.909, 80), angle = Angle(-37.841, 88.976, 1.023), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

SWEP.ZoomAmount         = 40
SWEP.ZoomTime           = 0.2


SWEP.HoldType = "melee"
SWEP.ViewModelFOV = 15
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_stunbaton.mdl"
SWEP.WorldModel = "models/weapons/w_stunbaton.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true


function SWEP:PrimaryAttack(worldsnd)

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
   if not self:CanPrimaryAttack() then return end

    

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, 511 )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), 511)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end


function SWEP:CreateZombie( ply, owner )

		if not IsValid( ply ) or not IsValid( owner ) then print("Not valid zombie") return end
		
		local ent = ply
		local weapon = owner:GetActiveWeapon()
		local edata = EffectData()

	  edata:SetEntity(ent)
	  edata:SetMagnitude(10)
	  edata:SetScale(25)
           
          util.Effect("ManhackSparks", edata)
          --ent:EmitSound( "weapons/gb_weapons/poseidon_bubble.mp3", 500 )

        local soundfile = gb_config.websounds.."/weapons/poseidon_bubble.mp3"
        gb_PlaySoundFromServer(soundfile,ent)

          timer.Create("riverforPoseidon", 0.5,3, function()
            if !IsValid(ent) then return end
            -- ent:EmitSound( "weapons/gb_weapons/poseidon_bubble.mp3", 500 )
            local soundfile = gb_config.websounds.."/weapons/poseidon_bubble.mp3"
            gb_PlaySoundFromServer(soundfile,ent)
          end)

          timer.Create("SparkiesForZeus",0.5,3,function()
                if not IsValid( ent ) then  return end
                edata:SetEntity(ent)
                edata:SetMagnitude(10)
                edata:SetScale(25)
                util.Effect("ManhackSparks", edata)
          end)

          timer.Create("EndBOOM",2.5,1,function()
                if not IsValid( ent ) then  return end
                edata:SetEntity(ent)
                edata:SetMagnitude(50)
                edata:SetScale(256)
               -- ent:EmitSound( "weapons/gb_weapons/poseidon_bubble.mp3", 510 )
                local soundfile = gb_config.websounds.."/weapons/poseidon_bubble.mp3"
                gb_PlaySoundFromServer(soundfile,ent)
                util.Effect("TeslaHitBoxes", edata)
                util.Effect("ManhackSparks", edata)
          end)

		
	  if SERVER and ent:IsPlayer() then
	  
			local randmodel = "models/props/cs_militia/fishriver01.mdl"			
			
			local oldPos = ent:GetPos() 
		  
			timer.Simple(2.9, function() 
						if IsValid( ent ) then
							spot = ent:GetPos() 
						else
							spot = oldPos
						end
							SpawnProp(spot, randmodel)
                                                        SpawnProp(spot, randmodel)
                                                        SpawnProp(spot, randmodel)
			end)
			
			timer.Simple(3,function() 
				if SERVER then
					if IsValid( ent ) and IsValid(self.Owner) then
                                            local dmg = DamageInfo()
                                            dmg:SetDamage(2000)
                                            dmg:SetAttacker(self.Owner)
                                            dmg:SetInflictor(self.Weapon)
                                            dmg:SetDamageType(DMG_DROWN)

                                            ent:TakeDamageInfo(dmg)
					   
				    end
					--local ply = self.Owner -- The first entity is always the first player
					--zed:SetPos(spot) -- This positions the zombie at the place our trace hit.
					--zed:SetHealth( 2000 )
					--zed:Spawn() -- This method spawns the zombie into the world, run for your lives! ( or just crowbar it dead(er) )
					--zed:SetHealth( 2000 )
				end
			end) --20
			timer.Create("infectedDamagezeus",0.25,20,function()
			  if IsValid( ent ) and IsValid(self.Owner) then
                          
                        local dmg = DamageInfo()
                                 dmg:SetDamage(3)
                                 dmg:SetAttacker(self.Owner)
                                 dmg:SetInflictor(self.Weapon)
                                 dmg:SetDamageType(DMG_DROWN)

                                 ent:TakeDamageInfo(dmg)

			  end
			end)

	  end


end

function SpawnProp(position, model)
	local ent1 = ents.Create("prop_physics") 
	local ang = Vector(0,0,1):Angle();
	ang.pitch = ang.pitch + 90;
	print("Prop spawned with model: " .. model)
	ang:RotateAroundAxis(ang:Up(), math.random(0,360))
	ent1:SetAngles(ang)
	ent1:SetModel(model)
	local pos = position
	pos.z = pos.z - ent1:OBBMaxs().z
	ent1:SetPos( pos )
	ent1:Spawn()
        
end 


function SWEP:ShootBullet( dmg, recoil, numbul, cone )

   local sights = self:GetIronsights()

   numbul = numbul or 1
   cone   = cone   or 0.01

   -- 10% accuracy bonus when sighting
   cone = sights and (cone * 0.9) or cone

   local bullet = {}
   bullet.Num    = numbul
   bullet.Src    = self.Owner:GetShootPos()
   bullet.Dir    = self.Owner:GetAimVector()
   bullet.Spread = Vector( cone, cone, 0 )
   bullet.Tracer = 4
   bullet.Force  = 5
   bullet.Damage = dmg

   bullet.Callback = function(att, tr, dmginfo)
		if SERVER or (CLIENT and IsFirstTimePredicted()) then

			if (not tr.HitWorld)  then
		       local pl2 = 0
				 
				    pl2 = tr.Entity
				 
					
				  self:CreateZombie( pl2, self.Owner )
			end	
		end
    end
	   self.Owner:FireBullets( bullet )
	   self.Weapon:SendWeaponAnim(self.PrimaryAnim)

	   -- Owner can die after firebullets, giving an error at muzzleflash
	   if not IsValid(self.Owner) or not self.Owner:Alive() then return end

	   self.Owner:MuzzleFlash()
	   self.Owner:SetAnimation( PLAYER_ATTACK1 )

	   if self.Owner:IsNPC() then return end

	   if ((game.SinglePlayer() and SERVER) or
		   ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted() )) then

		  -- reduce recoil if ironsighting
		  recoil = sights and (recoil * 0.5) or recoil

	   end
	
end


function SWEP:SetZoom(state)
    if CLIENT or !IsValid(self.Owner) then 
       return
    else
       if state && IsValid(self.Owner) then
         self.Owner:SetFOV(20, 0.3)
       else
          self.Owner:SetFOV(0, 0.2)
       end
    end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end
    
    bIronsights = not self:GetIronsights()
    
    self:SetIronsights( bIronsights )
    
    if SERVER then
        self:SetZoom(bIronsights)
     else
        self:EmitSound(self.Secondary.Sound)
    end
    
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:OnDrop()
    self:Holster()
    self:SetZoom(false)
    self:SetIronsights(false)
    
    return self.BaseClass.OnDrop(self)
end

function SWEP:PreDrop()
    self:Holster()
    self:SetZoom(false)
    self:SetIronsights(false)
    
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:OnRemove()
	self:Holster()
        return self.BaseClass.OnRemove(self)
end

function SWEP:Holster()
    
    if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	
   self:SetIronsights(false)
    self:SetZoom(false)
return self.BaseClass.Holster(self)
end

if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         -- crosshair
         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )


         -- cover edges
         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)
 
         -- scope
         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end

