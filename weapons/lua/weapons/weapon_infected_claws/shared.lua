

if SERVER then
   AddCSLuaFile( "shared.lua" )

--resource.AddFile("materials/vgui/ttt_fun_killicons/zombie_claws.png")

resource.AddFile("materials/models/weapons/v_zombiearms/zombie_classic_sheet.vtf")
resource.AddFile("materials/models/weapons/v_zombiearms/zombie_classic_sheet.vmt")
resource.AddFile("materials/models/weapons/v_zombiearms/zombie_classic_sheet_normal.vtf")
resource.AddFile("models/weapons/v_zombiearms.dx90.vtx")
resource.AddFile("models/weapons/v_zombiearms.sw.vtx")
resource.AddFile("models/weapons/v_zombiearms.dx80.vtx")
resource.AddFile("models/weapons/v_zombiearms.vvd")
resource.AddFile("models/weapons/v_zombiearms.mdl")


end

SWEP.Kind = WEAPON_EQUIP2   
SWEP.HoldType = "knife"
   SWEP.PrintName    = "Infected Claws"
   SWEP.Slot         = 6

if CLIENT then


  
   SWEP.ViewModelFlip = false

 

   SWEP.Icon = "vgui/ttt_fun_killicons/zombie_claws.png"
end

SWEP.Base               = "weapon_tttbase"

SWEP.ViewModel          = "models/weapons/v_zombiearms.mdl"
SWEP.WorldModel         = ''
SWEP.ShowWorldModel     = false 

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 0
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0.1
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 0.1
SWEP.AllowDrop = false




SWEP.IsSilent = true


function SWEP:OnDrop()
    self:Remove();
end

-- Pull out faster than standard guns
SWEP.DeploySpeed = 1
function SWEP:Think()
    if not self.NextHit or CurTime() < self.NextHit then return end
    self.NextHit = nil

    local pl = self.Owner
    local ent
    local tracedata = {}

tracedata.start = self.Owner:GetShootPos()
tracedata.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 100 )
tracedata.filter = self.Owner
tracedata.mins = Vector( -30,-30,-30 )
tracedata.maxs = Vector( 30,30,30 )
tracedata.mask = MASK_SHOT_HULL
local trace = util.TraceHull( tracedata )
   
    ent = trace.Entity
if IsValid(ent) then
    

            if  SERVER and (ent:IsPlayer() and ent:Alive() and ent:GetRole() != ROLE_TRAITOR) then

                      local spos = self.Owner:GetShootPos()
                    local sdest = spos + (self.Owner:GetAimVector() * 70)
                local edata = EffectData()
                    edata:SetStart(spos)
                    edata:SetOrigin(trace.HitPos)
                    edata:SetNormal(trace.Normal)
                    edata:SetEntity(ent)

                    if ent:IsPlayer() or ent:GetClass() == "prop_ragdoll" then
                       util.Effect("BloodImpact", edata)
                    end

               ent:SetRole(ROLE_TRAITOR)
                ent:SetCredits(2)
                ent.drawHalo        = false
                ent:SetHealth(150);
                ent:StripWeapons()
                ent:Give("weapon_infected_claws") 
                ent.hasProtectionSuit = true
                ent.CannotPickupWeapons = true
                ent:PrintMessage( HUD_PRINTTALK, "THE INFECTED: You just got INFECTED, Infect them ALL with your claws")
                ent:SetModel( "models/player/zombie_classic.mdl" )
                ent:SetModelScale(1, 0.02) 
                SendFullStateUpdate()
                pl:SetHealth(pl:Health()+25)
        end


    end
end

SWEP.NextSwing = 0
function SWEP:PrimaryAttack()
    if CurTime() < self.NextSwing then return end
	self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)
    self.Owner:DoAnimationEvent(PLAYER_ATTACK1)
    self.Owner:EmitSound("npc/zombie/zo_attack"..math.random(1, 2)..".wav")
    self.NextSwing = CurTime() + self.Primary.Delay
    self.NextHit = CurTime() + 0.4
    local vStart = self.Owner:EyePos() + Vector(0, 0, -40)
    local trace = util.TraceLine({start=vStart, endpos = vStart + self.Owner:GetAimVector() * 65, filter = self.Owner, mask = MASK_SHOT})
    if trace.HitNonWorld then
        self.PreHit = trace.Entity
        v = trace.Entity
        if IsValid( v ) and v:IsPlayer() then
            if SERVER then
                
            end
        end
    end
end

SWEP.NextMoan = 0
function SWEP:SecondaryAttack()
     
   
         
         self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
         self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)
         
         if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * 800 + Vector(0,0,200)) end
         
        if CurTime() < self.NextMoan then return end
        if SERVER and not CLIENT then
            self.Owner:EmitSound("npc/zombie/zombie_voice_idle"..math.random(1, 14)..".wav")
        end
        self.NextMoan = CurTime() + 3
end


function SWEP:Initialize()
    self:SetHoldType( self.HoldType or "pistol" )
    self:SetDeploySpeed(1)
end

function SWEP:Deploy()
   return true;
end

function SWEP:Precache()
    util.PrecacheSound("npc/zombie/zombie_voice_idle1.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle2.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle3.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle4.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle5.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle6.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle7.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle8.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle9.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle10.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle11.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle12.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle13.wav")
    util.PrecacheSound("npc/zombie/zombie_voice_idle14.wav")
    util.PrecacheSound("npc/zombie/claw_strike1.wav")
    util.PrecacheSound("npc/zombie/claw_strike2.wav")
    util.PrecacheSound("npc/zombie/claw_strike3.wav")
    util.PrecacheSound("npc/zombie/claw_miss1.wav")
    util.PrecacheSound("npc/zombie/claw_miss2.wav")
    util.PrecacheSound("npc/zombie/zo_attack1.wav")
    util.PrecacheSound("npc/zombie/zo_attack2.wav")
end