
local function FakeReplicatedConvar(name, default)
	if SERVER then
		CreateConVar(name, default, { FCVAR_ARCHIVE, FCVAR_GAMEDLL })
		SetGlobalString(name, GetConVar(name):GetString())
		cvars.AddChangeCallback(name, function(cvar, prev, newvalue)
			SetGlobalString(name, newvalue)
		end)
	end
end

local function GetFakeRepCvarNumber(name)
	return tonumber(GetGlobalString(name)) or 0
end

local functio

if SERVER then
	AddCSLuaFile(  )
	--resource.AddFile("materials/vgui/ttt/icon_rigger2.vmt")
	CreateConVar( "ttt_rigger_explosionmagnitude", "120", { FCVAR_ARCHIVE } )
	CreateConVar( "ttt_rigger_ragexplosionmul", "1.2", { FCVAR_ARCHIVE } )
end

FakeReplicatedConvar( "ttt_rigger_showfellows", "0", { FCVAR_REPLICATED, FCVAR_ARCHIVE } )
FakeReplicatedConvar( "ttt_rigger_props", "1", { FCVAR_REPLICATED, FCVAR_ARCHIVE } )
FakeReplicatedConvar( "ttt_rigger_ragdolls", "1", { FCVAR_REPLICATED, FCVAR_ARCHIVE } )

SWEP.Author = "Wyozi"

SWEP.HoldType			= "ar2"

SWEP.PrintName = "Rigger"
	SWEP.Slot = 7

if CLIENT then
	

	SWEP.ViewModelFlip = false
	SWEP.ViewModelFOV = 54

	local rprops, rrags = GetFakeRepCvarNumber("ttt_rigger_props") == 1, GetFakeRepCvarNumber("ttt_rigger_ragdolls") == 1

	local eqdesc = ""
	if rprops and rrags then
		eqdesc = "Left click to rig either a body (takes 2 ammo)\nor a prop."
	elseif rprops then
		eqdesc = "Left click to rig a prop."
	elseif rrags then
		eqdesc = "Left click to rig a ragdoll (takes all ammo)."
	end

	SWEP.EquipMenuData = {
		type = "item_weapon",
		desc = "Rig props and corpses with explosives.\n\n" .. eqdesc
	};

	
end

SWEP.Icon = "vgui/ttt/icon_rigger2"

SWEP.Base = "weapon_tttbase"
SWEP.Primary.Recoil	= 0.1
SWEP.Primary.Delay = 2.0
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 2
SWEP.Primary.DefaultClip = 2
SWEP.Primary.ClipMax = 2
SWEP.Primary.Ammo = "Rigger Ammo"
SWEP.Primary.Automatic = false

SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false

SWEP.Kind = WEAPON_EQUIP2
SWEP.CanBuy = {ROLE_TRAITOR} -- only traitors can buy
SWEP.LimitedStock = true -- only buyable once

SWEP.UseHands = true  

SWEP.ViewModel	= "models/weapons/c_IRifle.mdl"
SWEP.WorldModel	= "models/weapons/w_IRifle.mdl"

SWEP.NoSights = true

local maxrange = 400

local math = math

local function ValidEntTarget(ent) 
	return IsValid(ent) and ent:GetPhysicsObject() and (not ent:IsWeapon())
end

-- Returns if an entity is a valid physhammer punching target. Does not take
-- distance into account.
local function ValidPropTarget(ent)
	return ent:GetClass() == "prop_physics" and ent:Health() ~= 0 and GetFakeRepCvarNumber("ttt_rigger_props") == 1
end

local function ValidRagTarget(ent)
	return ent:GetClass() == "prop_ragdoll" and GetFakeRepCvarNumber("ttt_rigger_ragdolls") == 1
end

if SERVER then

	local function explode(owner, prop, israg)

		local ent = ents.Create( "env_explosion" )
			ent:SetPos( prop:GetPos() )
			ent:SetOwner( owner )
			ent:Spawn()

			ent.m_hInflictor = owner:GetWeapon("weapon_ttt_rigger")
			
			local magnitude = GetConVarNumber("ttt_rigger_explosionmagnitude")
			if israg then
				local mul = GetConVarNumber("ttt_rigger_ragexplosionmul")
				if mul ~= 0 then
					magnitude = magnitude * mul
				end
			end
			
			ent:SetKeyValue( "iMagnitude", tostring(magnitude) )
			ent:Fire( "Explode", 0, 0 )
			ent:EmitSound( "weapons/big_explosion.mp3" )

		DamageLog("RIGGER: " .. tostring(prop) .. " rigged by " .. owner:Nick() .. " [" .. owner:GetRoleString() .. "] exploded.")

	end

	local function PropBreak(breaker,prop)
		local rigger = prop:GetNWEntity("RiggedBy")
		if prop:IsValid() and rigger and rigger:IsValid() then
		
			prop:SetNWEntity("RiggedBy", NULL)

			explode(rigger, prop, false)
			
		end 
	end
	hook.Add("PropBreak","WTTRiggerPropBreak",PropBreak)

	hook.Add("InitPostEntity", "WTTTRiggerOverrideCorpseSearch", function()
		if not CORPSE or not KARMA then
			ErrorNoHalt("[WYOZI-TTT] Apparently not running Trouble in Terrorist Town. Not overriding methods.")
			return
		end
		local oldfunccs, oldfunckh = CORPSE.ShowSearch, KARMA.Hurt
		if not oldfunccs or not oldfunckh then -- Gamemode not TTT?
			ErrorNoHalt("[WYOZI-TTT] Function overrider failed! " .. tostring(oldfunccs) .. " " .. tostring(oldfunckh))
			return
		end
		function CORPSE.ShowSearch(ply, rag, covert, long_range)
			if not IsValid(ply) or not IsValid(rag) or rag:IsOnFire() then return end

			local rigger = rag:GetNWEntity("RiggedBy")
			if ply:IsTerror() and ply:Alive() and rigger and rigger:IsValid() then
				rag:SetNWEntity("RiggedBy", NULL)

				net.Start("WTTRiggerWarning")
					net.WriteEntity(rag)
					net.WriteBit(false)
				net.Send(GetTraitorTbl())

				explode(rigger, rag, true)
				return
			end

			oldfunccs(ply, rag, covert, long_range)
		end
		function KARMA.Hurt(attacker, victim, dmginfo)
			if not IsValid(attacker) or not IsValid(victim) then return end
			if attacker:IsTraitor() and victim:IsTraitor() and dmginfo:IsExplosionDamage() then
				local infl = dmginfo:GetInflictor()
				if IsValid(infl) and infl:GetClass() == "weapon_ttt_rigger" then
					return
				end
			end
			oldfunckh(attacker, victim, dmginfo)
		end
		MsgN("[WYOZI-TTT] Function overrider finished.")
	end)

	function GetTraitorTbl()
		local t = {}
		for k, v in pairs(player.GetAll()) do
			if IsValid(v) and v:GetTraitor() and v:Alive() then
				table.insert(t, v)
			end
		end
		return t
	end

	util.AddNetworkString("WTTRiggerWarning")
end

function SWEP:TraceLineRigger(ply) 
	return util.TraceLine({start=ply:GetShootPos(), endpos=ply:GetShootPos() + ply:GetAimVector() * maxrange, filter={ply, self.Entity}, mask=MASK_SOLID})
end


function SWEP:PrimaryAttack()
	if not self:CanPrimaryAttack() then return end
	self.Weapon:SetNextPrimaryFire(CurTime() + 1)

	if SERVER then

		local ply = self.Owner
		if not IsValid(ply) then return end

		local tr = self:TraceLineRigger(ply)
		if not IsValid(tr.Entity) then return end

		if ValidPropTarget(tr.Entity) or (ValidRagTarget(tr.Entity) and self:Clip1() >= 2) then
			tr.Entity:SetNWEntity("RiggedBy", ply)

			self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
			self:TakePrimaryAmmo(ValidRagTarget(tr.Entity) and 2 or 1)

			tr.Entity:EmitSound("weapons/debris2.wav")
			self:CallOnClient("StartRigCheck", "justrigged")
			DamageLog("RIGGER: " .. ply:Nick() .. " [" .. ply:GetRoleString() .. "] rigged " .. tostring(tr.Entity) .. ".")

			net.Start("WTTRiggerWarning")
				net.WriteEntity(tr.Entity)
				net.WriteBit(true)
			net.Send(GetTraitorTbl())

		else
			ply:SendLua([[ surface.PlaySound("common/wpn_denyselect.wav") ]])
		end

	end
end

function SWEP:StartRigCheck(wasJustRigged)

	local tr = self:TraceLineRigger(self.Owner)
	if IsValid(tr.Entity) and (ValidRagTarget(tr.Entity) or ValidPropTarget(tr.Entity)) then
		tr.Entity.LastRCheckStatus = wasJustRigged and self.Owner or tr.Entity:GetNWEntity("RiggedBy")
		tr.Entity.RigChecked = CurTime() + 1.5
	end
end

function SWEP:SecondaryAttack()

	if not self:CanSecondaryAttack() then return end
	self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)

	if CLIENT then
		self:StartRigCheck()
		surface.PlaySound("common/wpn_moveselect.wav")
	end

end

if CLIENT then
	
	local rigger_outline = Material("models/shadertest/shader4")
	
	function SWEP:ViewModelDrawn()

		local ply = LocalPlayer()
        local vm = ply:GetViewModel()
        if not IsValid(vm) then return end
	
		local tr = self:TraceLineRigger(ply)
		local tgt = tr.Entity

		if not IsValid(tgt) then return end
		
		if not ValidPropTarget(tr.Entity) and not ValidRagTarget(tr.Entity) then return end
		
		cam.Start3D(EyePos(), EyeAngles())

            render.MaterialOverride(rigger_outline)
            render.SuppressEngineLighting(true)

            tgt:SetModelScale(1.1, 0)
            tgt:DrawModel()

            render.SuppressEngineLighting(false)
            render.MaterialOverride(nil)

		cam.End3D()

		cam.Start2D()

			local stxt, sclr

			if tgt.RigChecked and tgt.RigChecked < CurTime() then
				local status = tgt.LastRCheckStatus
				if status == LocalPlayer() then
					stxt = "Rigged by you!"
					sclr = COLOR_GREEN
				elseif IsValid(status) then
					stxt = "Rigged by someone else"
					sclr = COLOR_RED
				else
					stxt = "Not rigged"
					sclr = Color(100, 100, 100)
				end
				stxt = stxt .. "(checked " .. tostring(math.Round(CurTime()-tgt.RigChecked)) .. "s ago)"
			elseif tgt.RigChecked then
				stxt = "Checking..." .. tostring(math.Round((tgt.RigChecked - CurTime()) * 100))
				sclr = COLOR_BLUE
			else
				stxt = "Unknown (right click to check)"
				sclr = COLOR_WHITE
			end

	        draw.DrawText(stxt, "TabLarge", ScrW()/2 + 10, ScrH()/2 - 25, sclr, TEXT_ALIGN_LEFT )
	        draw.DrawText((ValidRagTarget(tgt) and "R" or "E") .. tostring(tgt:EntIndex()), "TabLarge", ScrW()/2 - 8, ScrH()/2 - 25, Color(0, 127, 0, 255), TEXT_ALIGN_RIGHT )

		cam.End2D()
	end

	RiggerWarnings = RiggerWarnings or {}

	net.Receive("WTTRiggerWarning", function()
		local ent, bit = net.ReadEntity(), (net.ReadBit() == 1)
		if bit then
			table.insert(RiggerWarnings, ent)
		else
			table.RemoveByValue(RiggerWarnings, ent)
		end
	end)

	hook.Add("HUDPaint", "WTTTRiggerDrawNotifications", function()
		local cvar = GetFakeRepCvarNumber("ttt_rigger_showfellows")
		if cvar ~= 1 then return end
		if not LocalPlayer().IsActiveTraitor or not LocalPlayer():IsActiveTraitor() then return end

		for _,tgt in pairs(RiggerWarnings) do
			if not IsValid(tgt) then continue end

			local scrpos = tgt:GetPos():ToScreen()

			scrpos.x = math.Clamp(scrpos.x, 0, ScrW() - 0)
			scrpos.y = math.Clamp(scrpos.y, 0, ScrH() - 0)

			draw.DrawText("Rigged", "TabLarge", scrpos.x, scrpos.y, Color(255, 127, 0), TEXT_ALIGN_LEFT )
		end

	end)

	--[[
	local riggerspec_outline = Material("models/props_combine/com_shield001a")

	hook.Add("PostDrawTranslucentRenderables", "WTTTRiggerDrawHighlights", function()
		local cvar = GetConVar("ttt_rigger_showfellows")
		if not cvar or not cvar:GetBool() then return end
		if not LocalPlayer():IsActiveTraitor() then return end

		for _,tgt in pairs(RiggerWarnings) do
			if not IsValid(tgt) then continue end

			cam.Start3D(EyePos(), EyeAngles())
			cam.IgnoreZ(true)

	            render.MaterialOverride(riggerspec_outline)
	            render.SuppressEngineLighting(true)
	            render.SetColorModulation(1, 0.25, 0)


				tgt:SetModelScale(1.05, 0)
				tgt:DrawModel()

				render.SetColorModulation(1, 1, 1)
	            render.SuppressEngineLighting(false)
	            render.MaterialOverride(nil)

	        cam.IgnoreZ(false)
			cam.End3D()
		end
	end)
	]]

	hook.Add("TTTEndRound", "ClearRigWarnings", function()
		table.Empty(RiggerWarnings)
	end)

end