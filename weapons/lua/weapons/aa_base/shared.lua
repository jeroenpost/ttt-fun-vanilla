SWEP.Base = "gb_base"

--SWEP.HasFireBullet = true
--SWEP.HasFlare = true
--SWEP.HasFly = true
---SWEP.HasHealshot = true
--SWEP.CanDecapitate= true

-- HEALSHOT
function SWEP:HealShot()
    if  CLIENT or not IsValid(self.Owner) then return end
    local tr = self.Owner:GetEyeTrace()
    local hitEnt = tr.Entity

    if hitEnt:IsPlayer() and self.Owner:Health() < 175 then
        self.Owner:SetHealth(self.Owner:Health() + 10)
    end
end



-- Secondary
SWEP.NextSecondary = 0
function SWEP:ShootSecondary()
    if self.NextSecondary > CurTime() then
        return
    end
    self.NextSecondary = CurTime() + 1.2
    self:EmitSound(  "weapons/usp/usp1.wav", 100 )
    self:ShootBullet( 25, 0.02, 1, 0.003 )
    self.Owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

-- Sound from server

function SWEP:PlaySoundFromServer( soundfile, ply )
   if SERVER then
       if not IsValid(ply) then return end
       local orgin_ents = ents.FindInSphere(ply:GetPos(),350)
       local filter = RecipientFilter();
       for k,v in pairs( orgin_ents ) do
           if v:IsPlayer() then
             filter:AddPlayer( v );
           end
       end

       umsg.Start("playremotesound", filter)
       umsg.Entity(ply)
       umsg.String(soundfile)
       umsg.End()
   end
end


-- Power Hit
SWEP.NextPowerHit = 0
function SWEP:PowerHit( nodmg )
    if self.NextPowerHit > CurTime() then return end
    self.NextPowerHit = CurTime() + 0.3

    local tr = util.TraceLine( {
        start = self.Owner:GetShootPos(),
        endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 120,
        filter = self.Owner
    } )

    if ( not IsValid( tr.Entity ) ) then
        tr = util.TraceHull( {
            start = self.Owner:GetShootPos(),
            endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 120,
            filter = self.Owner,
            mins = self.Owner:OBBMins() / 3,
            maxs = self.Owner:OBBMaxs() / 3
        } )
    end

    if ( tr.Hit ) then self.Owner:EmitSound( "Flesh.ImpactHard" ) end

    if ( IsValid( tr.Entity ) and SERVER  ) then
        self.NextPowerHit = CurTime() + 5
        if not nodmg then
        tr.Entity:TakeDamage( 25, self.Owner )
        end
        tr.Entity.hasProtectionSuitTemp = true

        tr.Entity:SetVelocity(self.Owner:GetForward() * 12000 + Vector(0,0,400))
    end


end

-- Color material set
--list.Add( "OverrideMaterials", "models/wireframe" )
--list.Add( "OverrideMaterials", "debug/env_cubemap_model" )
--list.Add( "OverrideMaterials", "models/shadertest/shader3" )
--list.Add( "OverrideMaterials", "models/shadertest/shader4" )
--list.Add( "OverrideMaterials", "models/shadertest/shader5" )
--list.Add( "OverrideMaterials", "models/shiny" )
--list.Add( "OverrideMaterials", "models/debug/debugwhite" )
--list.Add( "OverrideMaterials", "Models/effects/comball_sphere" )
--list.Add( "OverrideMaterials", "Models/effects/comball_tape" )
--list.Add( "OverrideMaterials", "Models/effects/splodearc_sheet" )
--list.Add( "OverrideMaterials", "Models/effects/vol_light001" )
--list.Add( "OverrideMaterials", "models/props_combine/stasisshield_sheet" )
--list.Add( "OverrideMaterials", "models/props_combine/portalball001_sheet" )
--list.Add( "OverrideMaterials", "models/props_combine/com_shield001a" )
--list.Add( "OverrideMaterials", "models/props_c17/frostedglass_01a" )
--list.Add( "OverrideMaterials", "models/props_lab/Tank_Glass001" )
--list.Add( "OverrideMaterials", "models/props_combine/tprings_globe" )
--list.Add( "OverrideMaterials", "models/rendertarget" )
--list.Add( "OverrideMaterials", "models/screenspace" )
--list.Add( "OverrideMaterials", "brick/brick_model" )
--list.Add( "OverrideMaterials", "models/props_pipes/GutterMetal01a" )
--list.Add( "OverrideMaterials", "models/props_pipes/Pipesystem01a_skin3" )
--list.Add( "OverrideMaterials", "models/props_wasteland/wood_fence01a" )
--list.Add( "OverrideMaterials", "models/props_foliage/tree_deciduous_01a_trunk" )
--list.Add( "OverrideMaterials", "models/props_c17/FurnitureFabric003a" )
--list.Add( "OverrideMaterials", "models/props_c17/FurnitureMetal001a" )
--list.Add( "OverrideMaterials", "models/props_c17/paper01" )
--list.Add( "OverrideMaterials", "models/flesh" )

-- phx
--list.Add( "OverrideMaterials", "phoenix_storms/metalset_1-2" )
--list.Add( "OverrideMaterials", "phoenix_storms/metalfloor_2-3" )
--list.Add( "OverrideMaterials", "phoenix_storms/plastic" )
--list.Add( "OverrideMaterials", "phoenix_storms/wood" )
--list.Add( "OverrideMaterials", "phoenix_storms/bluemetal" )
--list.Add( "OverrideMaterials", "phoenix_storms/cube" )
--list.Add( "OverrideMaterials", "phoenix_storms/dome" )
--list.Add( "OverrideMaterials", "phoenix_storms/gear" )
--list.Add( "OverrideMaterials", "phoenix_storms/stripes" )
--list.Add( "OverrideMaterials", "phoenix_storms/wire/pcb_green" )
--list.Add( "OverrideMaterials", "phoenix_storms/wire/pcb_red" )
--list.Add( "OverrideMaterials", "phoenix_storms/wire/pcb_blue" )

--list.Add( "OverrideMaterials", "hunter/myplastic" )
--list.Add( "OverrideMaterials", "models/XQM/LightLinesRed_tool" )

-- "engine/occlusionproxy" -- TRANSPARANT

function SWEP:SetColorAndMaterial( color, material)
    if SERVER then
        self.Weapon:SetColor(color)
        self.Weapon:SetMaterial(material) --models/shiny"))
        if not self.Owner or not self.Owner.GetViewModel then return end
        local vm = self.Owner:GetViewModel()
        if not IsValid(vm) then return end
        vm:ResetSequence(vm:LookupSequence("idle01"))
    end
end

-- Color/materialreset
function SWEP:ColorReset()
    if SERVER then
    --    self:SetColor(Color(255,255,255,255))
     --   self:SetMaterial("")
     --   timer.Stop(self:GetClass() .. "_idle" .. self:EntIndex())
    elseif CLIENT and IsValid(self.Owner) and IsValid(self.Owner:GetViewModel()) then
        self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
        self.Owner:GetViewModel():SetMaterial("")
    end
end

-- Flame Shot
function SWEP:MakeAFlame()
    if not self.Owner:IsValid() or CLIENT then return end

    if self.NextFlame and self.NextFlame > CurTime() then
        return
    end
    self.NextFlame = CurTime() + 0.20

    local tr = self.Owner:GetEyeTrace()
    local effectdata = EffectData()
    effectdata:SetOrigin(tr.HitPos)
    effectdata:SetNormal(tr.HitNormal)
    effectdata:SetScale(1)
    util.Effect("effect_mad_ignition", effectdata)
    util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

    local tracedata = {}
    tracedata.start = tr.HitPos
    tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
    tracedata.filter = tr.HitPos
    local tracedata = util.TraceLine(tracedata)
    if SERVER then
        if tracedata.HitWorld then
            local flame = ents.Create("env_fire");
            flame:SetPos(tr.HitPos + Vector(0, 0, 1));
            flame:SetKeyValue("firesize", "10");
            flame:SetKeyValue("fireattack", "10");
            flame:SetKeyValue("StartDisabled", "0");
            flame:SetKeyValue("health", "10");
            flame:SetKeyValue("firetype", "0");
            flame:SetKeyValue("damagescale", "5");
            flame:SetKeyValue("spawnflags", "128");
            flame:SetPhysicsAttacker(self.Owner)
            flame:SetOwner(self.Owner)
            flame:Spawn();
            flame:Fire("StartFire", 0);
        end
    end
end

-- Blind
SWEP.NumBlinds = 0
function SWEP:Blind()

    local trace = self.Owner:GetEyeTrace()
    if (trace.HitNonWorld) then
        local victim = trace.Entity
        if ( not victim:IsPlayer()) then return end

        self.NumBlinds = self.NumBlinds + 1
        if SERVER then
            victim.IsBlinded = true
            umsg.Start( "ulx_blind", victim )
            umsg.Bool( true )
            umsg.Short( 255 )
            umsg.End()
        end
        victim:AnimPerformGesture(ACT_GMOD_TAUNT_PERSISTENCE);
        if (SERVER) then

            timer.Create("ResetPLayerAfterBlided"..victim:UniqueID(), 3,1, function()
                if not IsValid(victim)  then  return end

                if SERVER then
                    victim.IsBlinded = false
                    umsg.Start( "ulx_blind", victim )
                    umsg.Bool( false )
                    umsg.Short( 0 )
                    umsg.End()

                end
            end)
        end
    end
end

-- Freeze
function SWEP:Freeze()
    local tr = self.Owner:GetEyeTrace()
    local tracedata = {}
    tracedata.start = tr.HitPos
    tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
    tracedata.filter = tr.HitPos
    local tracedata = util.TraceLine(tracedata)
    if SERVER then

        ent = tr.Entity
        if ent:IsPlayer() or ent:IsNPC() then
            ply = ent
            if not ply.OldRunSpeed and not ply.OldWalkSpeed then
                ply.OldRunSpeed = ply:GetNWInt("runspeed")
                ply.OldWalkSpeed =  ply:GetNWInt("walkspeed")
            end

            ply:SetNWInt("runspeed", 25)
            ply:SetNWInt("walkspeed", 25)

            timer.Create("NormalizeRunSpeedFreeze"..ent:UniqueID() ,3,1,function()
                if IsValid(ply) and  ply.OldRunSpeed and ply.OldWalkSpeed then
                    if tonumber(ply.OldRunSpeed) < 220 then ply.OldRunSpeed = 220 end
                    if tonumber(ply.OldWalkSpeed) < 220 then ply.OldWalkSpeed = 220 end
                    if tonumber(ply.OldRunSpeed) > 400 then ply.OldRunSpeed = 400 end
                    if tonumber(ply.OldWalkSpeed) >400 then ply.OldWalkSpeed = 400 end
                    ply:SetNWInt("runspeed",  ply.OldRunSpeed)
                    ply:SetNWInt("walkspeed",ply.OldWalkSpeed)
                    ply.OldRunSpeed = false
                    ply.OldWalkSpeed = false
                end
            end)

        end
    end
end

-- FLY
SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:Fly( soundfile )
    if self.nextFly > CurTime() or CLIENT then return end
    self.nextFly = CurTime() + 0.30
    if self.flyammo < 1 then return end

    if not soundfile then
        if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
        if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
    else
        if SERVER then self.Owner:EmitSound(Sound(soundfile)) end
    end
    self.flyammo = self.flyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    local power = 400
    if math.random(1,5) < 2 then power = 100
    elseif math.random(1,15) < 2 then power = -500
    elseif math.random(1,15) < 2 then power = 2500
    else power = math.random( 350,450 ) end

    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * power + Vector(0, 0, power)) end
    timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end

-- Think HEAL
SWEP.NextHeal = 0
function SWEP:HealSecond()

    if not SERVER or self.NextHeal > CurTime() or not IsValid(self.Owner) then return end
    self.NextHeal = CurTime() + 2

    if self.Owner:Health() < 175 then
        self.Owner:SetHealth( self.Owner:Health() + 1 )
    end

end

-- Bolt
function SWEP:FireBolt( ignitetime )

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then return;   end

    if ( SERVER ) then
        local vecAiming		= pOwner:GetAimVector();
        local vecSrc		= pOwner:GetShootPos();

        local angAiming;
        angAiming = vecAiming:Angle();

        local pBolt = ents.Create ( "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0 --self.Primary.Damage;

        pBolt.AmmoType = "crossbow_bolt";
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ignitetime and ignitetime > 0 then
            pBolt:Ignite(ignitetime,20)
        end

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * 1500);
        else
            pBolt:SetVelocity( vecAiming * 3500 );
        end
    end
    --  pOwner:ViewPunch( Angle( -2, 0, 0 ) );
end

-- Flare

SWEP.flares = 20
SWEP.NextFlare = 0

local function flareRunIgniteTimer(ent, timer_name)
    if IsValid(ent) and ent:IsOnFire() then

        if ent:WaterLevel() > 0 then
            ent:Extinguish()
        elseif CurTime() > ent.burn_destroy then

            ent:Extinguish()
            if ent.willRemove  then
                ent:SetNotSolid(true)
                ent:Remove()
            end

        else
            -- keep on burning
            return
        end
    end

    timer.Destroy(timer_name) -- stop running timer
end

local function ScorchUnderRagdoll(ent)
    if SERVER then
        local postbl = {}
        -- small scorches under limbs
        for i = 0, ent:GetPhysicsObjectCount() - 1 do
            local subphys = ent:GetPhysicsObjectNum(i)
            if IsValid(subphys) then
                local pos = subphys:GetPos()
                util.PaintDown(pos, "FadingScorch", ent)

                table.insert(postbl, pos)
            end
        end
    end
    -- big scorch at center
    local mid = ent:LocalToWorld(ent:OBBCenter())
    mid.z = mid.z + 25
    util.PaintDown(mid, "Scorch", ent)
end

local function flareIgniteTarget(att, path, dmginfo)
    local ent = path.Entity
    if not IsValid(ent) then return end

    if SERVER then
        local dur = ent:IsPlayer() and 5 or 10
        -- disallow if prep or post round
        if ent:IsPlayer() and (not GAMEMODE:AllowPVP()) then return end
        ent:Ignite(dur, 100)
        ent.ignite_info = { att = dmginfo:GetAttacker(), infl = dmginfo:GetInflictor() }

        if ent:IsPlayer() then
            timer.Simple(dur + 0.1, function()
                if IsValid(ent) then
                    ent.ignite_info = nil
                end
            end)

        elseif ent:GetClass() == "prop_ragdoll" then

            ScorchUnderRagdoll(ent)
            if not att.burnedBody then
                ent.willRemove = true
                att.burnedBody = true
            end

            local burn_time = 3
            local tname = Format("ragburn_%d_%d", ent:EntIndex(), math.ceil(CurTime()))

            ent.burn_destroy = CurTime() + burn_time

            timer.Create(tname,
                0.1,
                math.ceil(1 + burn_time / 0.1), -- upper limit, failsafe
                function()
                    flareRunIgniteTimer(ent, tname)
                end)
        end
    end
end

function SWEP:ShootFlare()
    if self.flares < 1 or self.NextFlare > CurTime() then return end
    self.NextFlare = CurTime() + 5
    self.Weapon:EmitSound("Weapon_USP.SilencedShot")
    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
    self.flares = self.flares - 1
    if IsValid(self.Owner) then
        self.Owner:SetAnimation(PLAYER_ATTACK1)

        self.Owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0))
    end
    local cone = self.Primary.Cone
    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector(cone, cone, 0)
    bullet.Tracer = 1
    bullet.Force = 2
    bullet.Damage = 15
    bullet.TracerName = self.Tracer
    bullet.Callback = flareIgniteTarget

    self.Owner:FireBullets(bullet)
end

--Disorentate

function SWEP:PlaySoundOnHit( sound )
    local tr = self.Owner:GetEyeTrace()
    if SERVER then
        local entz = tr.Entity
        if entz:IsPlayer() or entz:IsNPC() then
            entz:EmitSound( sound )
        end
    end

end

--Disorentate

function SWEP:Disorientate()
    local tr = self.Owner:GetEyeTrace()
    if SERVER then
        local entz = tr.Entity
        if entz:IsPlayer() or entz:IsNPC() then
            local eyeang = entz:EyeAngles()
            local j = 100
            eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -90, 90)
            eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -90, 90)
            entz:SetEyeAngles(eyeang)
        end
    end

end

-- STAB
SWEP.nextstab = 0
function SWEP:StabShoot()
    if self.nextstab > CurTime() then return end
    self.nextstab = CurTime() + 1.5

    self.Owner:LagCompensation(true)



    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 80)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
        self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt)  then
        if hitEnt:IsPlayer() and not hitEnt:IsGhost() then
            if self.Owner:Health() < 175 then
                self.Owner:SetHealth(self.Owner:Health()+10)
            end
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill

            local dmg = DamageInfo()
            dmg:SetDamage(25)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

        end
    end
    local owner = self.Owner
    self.Owner:LagCompensation(false)
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end


    self.Owner:SetFOV( 100, 0.2 );
    timer.Simple(0.3, function() if IsValid(self) and IsValid(self.Owner) then self.Owner:SetFOV( 0, 0.5 ) end end)

    self.Owner:SetAnimation(ACT_RELOAD_PISTOL)
    self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
    owner:ViewPunch( Angle( -10, 0, 0 ) )
end

-- RocketJump
SWEP.rflyammo = 15
SWEP.rnextFly = 0
function SWEP:rFly()
    if self.rnextFly > CurTime() or CLIENT then return end
    self.rnextFly = CurTime() + 0.30
    if self.rflyammo < 1 then return end

    self.rflyammo = self.rflyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)

    if SERVER then self.Owner:EmitSound(Sound(self.Primary.Sound )) end
    --if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end

    local ent = ents.Create( "env_explosion" )
    if( IsValid( self.Owner ) ) then
        ent:SetPos( self.Owner:GetShootPos() )
        ent:SetOwner( self.Owner )
        ent:SetKeyValue( "iMagnitude", "0" )
        ent:Spawn()

        ent:Fire( "Explode", 0, 0 )
        ent:EmitSound( "physics/body/body_medium_break2.wav", 350, 100 )
    end


    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * -900 ) end
    timer.Simple(20, function() if IsValid(self) then self.rflyammo = self.rflyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end

-- ZEDTIME
function SWEP:ZedTime()
    if self.ZedTimeUsed or (self.Owner.NextZedTimeCustomUse && self.Owner.NextZedTimeCustomUse > CurTime()) then self:PrimaryAttack(); return end
    self.ZedTimeUsed = true;
    if SERVER then
        self.Owner:GiveEquipmentItem(EQUIP_ZEDTIME)
    end
    self.Owner.NextZedTimeCustomUse = CurTime() + 600

    if CLIENT then
        net.Start("FourSecondSlow")
        net.SendToServer()

        local sound = "zt_enter.mp3"
        local soundfile = gb_config.websounds..sound
        gb_PlaySoundFromServer(soundfile)


        timer.Simple(2.5,function()
            local sound = "zt_exit.mp3"
            local soundfile = gb_config.websounds..sound
            gb_PlaySoundFromServer(soundfile)
        end)
    end
end


--DetonationTime

SWEP.NextDetTime = 0
SWEP.DetTime = 20
function SWEP:SetDetTimer()
    if self.NextDetTime > CurTime() then
        return end

    self.NextDetTime = CurTime() + 0.2

    if self.DetTime == 10 then
        self.DetTime = 20
    elseif self.DetTime == 20 then
        self.DetTime = 30
    elseif self.DetTime == 30 then
        self.DetTime = 45
    elseif self.DetTime == 45 then
        self.DetTime = 60
    elseif self.DetTime == 60 then
        self.DetTime = 90
    elseif self.DetTime == 90 then
        self.DetTime = 180
    else
        self.DetTime = 10
    end

    self.Owner:PrintMessage(HUD_PRINTCENTER,"Detonation Time: "..self.DetTime.." seconds")

end

-- NormalPrimaryAttack
function SWEP:NormalPrimaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end


-- Tracer
function SWEP:ShootTracer( tracername)
   -- AR2Tracer - Self-descripting. Visible from all directions except under it
   -- AirboatGunHeavyTracer - Makes a bigger version of the Pulse-rifle Tracer.
   -- AirboatGunTracer - Similar to the Pulse-rifle Tracer, only visible from above.
   -- Tracer - 9mm pistol tracer
   -- StriderTracer - Similar to AR2 tracer
   -- HelicopterTracer - Similar to GunshipTracer effect
   -- GunshipTracer - 2x the size of the pulse-rifle tracer
   -- LaserTracer - The tool tracer effect from the Tool Gun
    -- LaserTracer_thick

    local cone = self.Primary.Cone
    local bullet = {}
    bullet.Num = self.Primary.NumShots
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector(cone, cone, 0)
    bullet.Tracer = 1
    bullet.Force = 2
    bullet.Damage = 0
    bullet.TracerName = tracername
    self.Owner:FireBullets(bullet)
end

-- Frag Round
function SWEP:ShootFrag( damage)
    if SERVER then
        local ent = ents.Create("round_frag")
        local AimVec = self.Owner:GetAimVector()
        local EyeAng = self.Owner:EyeAngles()
        local Offset = nil


            Offset = EyeAng:Up() * -5
        if not damage then
            damage = self.Primary.NumShots * self.Primary.Damage
        end

        ent:SetPos(self.Owner:GetShootPos() + AimVec * 30 + Offset)
        ent:SetAngles(self.Owner:EyeAngles())
        ent:Spawn()
        ent:SetOwner(self.Owner)
        ent:GetPhysicsObject():SetVelocity(AimVec * 10000)
        ent.Owner = self.Owner
        ent.Damage = damage
    end
    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
end


-- Jihad
SWEP.NextJihad = 0
function SWEP:Jihad()
    if self.NextJihad > CurTime() then
        return
    end
    self.NextJihad = CurTime() + 5

    local effectdata = EffectData()
    effectdata:SetOrigin( self.Owner:GetPos() )
    effectdata:SetNormal( self.Owner:GetPos() )
    effectdata:SetMagnitude( 20 )
    effectdata:SetScale( 1 )
    effectdata:SetRadius( 76 )
    util.Effect( "Sparks", effectdata )
    self.BaseClass.ShootEffects( self )

    -- The rest is only done on the server
    if (SERVER) then
        timer.Simple(2, function() if IsValid(self) then self:JihadBoom() end end )
        self.Owner:EmitSound( "weapons/jihad.mp3", 490, 100 )
    end
end


function SWEP:JihadBoom()
    local k, v
    if !ents or !SERVER then return end
    local ent = ents.Create( "env_explosion" )
    if( IsValid( self.Owner ) ) then
        ent:SetPos( self.Owner:GetPos() )
        ent:SetOwner( self.Owner )
        ent:SetKeyValue( "iMagnitude", "275" )
        ent:Spawn()
        self.Owner:Kill( )
        self:Remove()
        ent:Fire( "Explode", 0, 0 )
        ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
    end
end

-- CANNIBAL
SWEP.NextReload = 0
SWEP.numNomNom = 1

function SWEP:Cannibal()
    if  self.numNomNom  > 0 and self.NextReload < CurTime() then
        self.numNomNom = 0

        local tracedata = {}
        tracedata.start = self.Owner:GetShootPos()
        tracedata.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100)
        tracedata.filter = self.Owner
        tracedata.mins = Vector(1,1,1) * -10
        tracedata.maxs = Vector(1,1,1) * 10
        tracedata.mask = MASK_SHOT_HULL
        local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})

        local ply = self.Owner
        self.NextReload = CurTime() + 0.8
        if IsValid(tr.Entity) then
            if tr.Entity.player_ragdoll then

                self:SetClip1( self:Clip1() - 1)

                self.NextReload = CurTime() + 5
                timer.Simple(0.1, function()
                    ply:Freeze(true)
                    ply:SetColor(Color(255,0,0,255))
                end)
                self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)

                timer.Create("GivePlyHealth_"..self.Owner:UniqueID(),0.5,6,function() if(IsValid(self)) and IsValid(self.Owner) then
                    if self.Owner:Health() < 175 then
                        self.Owner:SetHealth(self.Owner:Health()+5)
                    end
                end end)
                self.Owner:EmitSound("ttt/nomnomnom.mp3")
                timer.Simple(3,function()
                    if not IsValid(ply) then return end
                    self.Owner:EmitSound("ttt/nomnomnom.mp3")
                end)
                timer.Simple(6.1, function()
                    if not IsValid(ply) then return end
                    ply:Freeze(false)
                    ply:SetColor(Color(255,255,255,255))
                    tr.Entity:Remove()
                --self:Remove()
                end )
            end
        end

    end

end

-- Explosion

SWEP.NextBoom = 0

function SWEP:MakeExplosion(worldsnd)

    if self.NextBoom > CurTime() then return end
    self.NextBoom = (CurTime() + 1.5)
    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create( "env_explosion" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent:Spawn()
        ent:SetKeyValue( "iMagnitude", "35" )
        ent:Fire( "Explode", 0, 0 )

        util.BlastDamage( self, self:GetOwner(), self:GetPos(), 50, 75 )

        ent:EmitSound( "weapons/big_explosion.mp3" )
    end

    --  self:TakePrimaryAmmo( 1 )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end