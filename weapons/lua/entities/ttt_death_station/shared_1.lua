---- Death Station

if SERVER then 
AddCSLuaFile("shared.lua") 
end

if CLIENT then
   ENT.Icon = "vgui/ttt/icon_fon_death_station2"
   ENT.PrintName = "Death Station"
   ENT.Author = "Pwned (HF)"

   local GetPTranslation = LANG.GetParamTranslation

   ENT.TargetIDHint = {
      name = "hstation_name",
      hint = "hstation_hint",
      fmt  = function(ent, txt)
                return GetPTranslation(txt,
                                       { usekey = Key("+use", "USE"),
                                         num    = ent:GetStoredHealth() or 0 } )
             end
   };
   
  end

ENT.Type = "anim"
ENT.Model = Model("models/props/cs_office/microwave.mdl")

ENT.CanHavePrints = true
ENT.MaxHeal = 25
ENT.MaxStored = 200
ENT.RechargeRate = 1
ENT.RechargeFreq = 2

ENT.NextHeal = 0
ENT.HealRate = -50
ENT.HealFreq = 0.2

AccessorFuncDT(ENT, "StoredHealth", "StoredHealth")

AccessorFunc(ENT, "Placer", "Placer")

function ENT:SetupDataTables()
   self:DTVar("Int", 0, "StoredHealth")
end
   
function ENT:Initialize()
   self:SetModel(self.Model)
   self:SetOwner(ply)
   self:PhysicsInit(SOLID_VPHYSICS)
   self:SetMoveType(MOVETYPE_VPHYSICS)
   self:SetSolid(SOLID_BBOX)

   local b = 32
   self:SetCollisionBounds(Vector(-b, -b, -b), Vector(b,b,b))

   self:SetCollisionGroup(COLLISION_GROUP_WEAPON)
   if SERVER then
      self:SetMaxHealth(200)

      local phys = self:GetPhysicsObject()
      if IsValid(phys) then
         phys:SetMass(200)
      end

      self:SetUseType(CONTINUOUS_USE)
   end
   self:SetHealth(200)

   self:SetColor(Color(180, 180, 250, 255))

   self:SetStoredHealth(200)

   self:SetPlacer(nil)
   
   self.NextHeal = 0
   
   self.fingerprints = {}

   for k, v in pairs(player.GetAll()) do
    if v:IsTraitor()  then
		 self.TargetIDHint = {
      name = "Death Station",
      hint = "Don't Use.",};
end
end
end

function ENT:AddToStorage(amount)
   self:SetStoredHealth(math.min(self.MaxStored, self:GetStoredHealth() + amount))
end

function ENT:TakeFromStorage(amount)
   amount = math.min(amount, self:GetStoredHealth())
   self:SetStoredHealth(math.max(0, self:GetStoredHealth() - amount))
   return amount
end

local healsound = Sound("items/medshot4.wav")
local failsound = Sound("items/medshotno1.wav")

local last_sound_time = 0
function ENT:GiveHealth(ply, max_heal)
   if self:GetStoredHealth() > 0 then
      max_heal = max_heal or self.MaxHeal
      local dmg = ply:GetMaxHealth() - ply:Health()
      if dmg > 0 then
         -- constant clamping, no risks
         local healed = self:TakeFromStorage(math.min(max_heal, dmg))
         local new = math.min(ply:GetMaxHealth(), ply:Health() + healed)	 

         ply:SetHealth(new)

         if last_sound_time + 2 < CurTime() then
            self:EmitSound(healsound)
            last_sound_time = CurTime()
         end
		 
		 if ply:Health() < 0 then
            ply:TakeDamage(1)
			end

         if not table.HasValue(self.fingerprints, ply) then
            table.insert(self.fingerprints, ply)
         end

         return true
      else
         self:EmitSound(failsound)
      end
   else
      self:EmitSound(failsound)
   end

   return false
end

function ENT:Use(ply)
   if IsValid(ply) and ply:IsPlayer() and ply:IsActive() then
      local t = CurTime()
      if t > self.NextHeal then
         local healed = self:GiveHealth(ply, self.HealRate)

         self.NextHeal = t + (self.HealFreq * (healed and 1 or 2))
      end
   end
end

function ENT:OnTakeDamage(dmginfo)
   if dmginfo:GetAttacker() == self:GetPlacer() then return end

   self:TakePhysicsDamage(dmginfo)

   self:SetHealth(self:Health() - dmginfo:GetDamage())

   local att = dmginfo:GetAttacker()
   if IsPlayer(att) then
      DamageLog(Format("%s damaged death station for %d dmg",
                       att:Nick(), dmginfo:GetDamage()))
   end
   	 local bamm = ents.Create( "env_explosion" )
	
   if self:Health() < 0 then
      self:Remove()
	  
	 local bamm = ents.Create( "env_explosion" )
	bamm:SetPos( self:GetPos() + Vector (0,0,4))
	bamm:SetOwner( self.Owner )
	bamm:SetKeyValue( "iMagnitude", "125" )
	bamm:SetKeyValue( "rendermode", "4")
	bamm:Fire( "Explode", "", 0 )
	bamm:EmitSound( "weapons/big_explosion.mp3", 500, 500 )
	bamm:Spawn()

      local effect = EffectData()
      effect:SetOrigin(self:GetPos())
      util.Effect("Explosion_2_FireSmoke", effect)

      if IsValid(self:GetOwner()) then
         TraitorMsg(self:GetOwner(), "You Death Station has been Destroyed.")
      end
   end
end

if SERVER then
   -- recharge
   local nextcharge = 0
   function ENT:Think()
      if nextcharge < CurTime() then
         self:AddToStorage(self.RechargeRate)

         nextcharge = CurTime() + self.RechargeFreq
      end
   end
end