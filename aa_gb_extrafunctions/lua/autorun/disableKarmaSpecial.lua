if SERVER then

hook.Add("InitPostEntity", "greenBlackOverrideKarma", function()
		if not CORPSE or not KARMA then
			ErrorNoHalt("[TTT_FUN] Apparently not running Trouble in Terrorist Town. Not overriding methods.")
			return
		end
		local oldfunccs, oldfunckh = CORPSE.ShowSearch, KARMA.Hurt
		if not oldfunccs or not oldfunckh then -- Gamemode not TTT?
			ErrorNoHalt("[TTT_FUN-TTT] Function overrider failed! " .. tostring(oldfunccs) .. " " .. tostring(oldfunckh))
			return
		end
		function KARMA.Hurt(attacker, victim, dmginfo)
			if not IsValid(attacker) or not IsValid(victim) then return end
			if isSpecialRound or (attacker == victim) or victim:IsBot() then
                            return   
                        end
			oldfunckh(attacker, victim, dmginfo)
		end
		MsgN("[TTT_FUN] Function overrider finished.")
	end)

end