

--Check if ply is admin
--Check if ply is admin




if SERVER then


    -- Allow an admin to talk while death
    function playerAdminTalk( listener, talker )
        if IsValid(talker) and (talker.IsAbleToTalkWhileDeath or gb_mapvote.started) then
            return true
        end
    end

    hook.Add("PlayerCanHearPlayersVoice","enableAdminTalkingDeath", playerAdminTalk)


    util.AddNetworkString("admin_getDamageLogFromServer")
    util.AddNetworkString("admin_getDamageLog")


    net.Receive( "admin_getDamageLogFromServer", function(len,ply)

        if not gb.is_jrmod(ply) and GetRoundState() == ROUND_ACTIVE then return end

        local damagelog = ''
        local good = "<span style='color:green;'>"
        local hgood = "<span style='color:orange;'>"
        local bad = "<span style='color:red;'>"
        local lelse = "<span style='color:blue;'>"
        local lend = "</span><br/>"

        local lastline = ""
        local skip = 0
        if #GAMEMODE.DamageLog > 500 then
            skip = #GAMEMODE.DamageLog - 500
        end

        for k, txt in ipairs(GAMEMODE.DamageLog) do

            local killl = string.find( txt, "KILL")

            if k < skip then
                continue
                end

                lastline = txt




                txt = string.gsub(txt, "<", "_")
                txt = string.gsub(txt, ">", "_")

                if killl == 12 then
                    good = "<div class='good'>"
                    hgood = "<div class='hgood'>"
                    bad = "<div class='bad'>"
                    lelse = "<div class='lelse'>"
                    lend = "</div>"
                else
                    good = "<span style='color:#1E5520;'>"
                    hgood = "<span style='color:#53551E;'>"
                    bad = "<span style='color:#551E1E;'>"
                    lelse = "<span style='color:#1E1E55;'>"
                    lend = "</span><br/>"
                end
                local innocent2 = false
                local detective2 = false
                local traitor2 =  false

                local propkill = string.find( txt, "%<something%/world%>")
                local innocent = string.find(  txt, "%[innocent%]");
                if innocent then innocent2 = string.find( txt, "%[innocent%]", innocent+10) end
                local detective = string.find( txt, "%[detective%]");
                if detective then detective2 = string.find( txt, "%[detective%]", detective+12) end
                local traitor = string.find( txt, "%[traitor%]");
                if traitor then traitor2 = string.find( txt, "%[traitor%]", traitor+9) end

                if     propkill                 then damagelog = damagelog..good..string.gsub(string.gsub(txt,">","@]"),"<","@[")..lend
                elseif innocent  and innocent2  then damagelog = damagelog..bad..txt..lend
                elseif traitor   and traitor2   then damagelog = damagelog..bad..txt..lend
                elseif detective and detective2 then damagelog = damagelog..bad..txt..lend
                elseif innocent  and detective  then damagelog = damagelog..bad..txt..lend
                elseif innocent  and traitor    and (innocent > traitor) then damagelog = damagelog..good..txt..lend
                elseif innocent  and traitor    and (innocent < traitor) then  damagelog = damagelog..hgood..txt..lend
                elseif traitor   and detective  and (detective > traitor) then damagelog = damagelog..good..txt..lend
                elseif traitor   and detective  and (detective < traitor) then  damagelog = damagelog..hgood..txt..lend
                else damagelog = damagelog..lelse..txt..lend end

            end


            damagelog = "<style>lelse{color:#050587;font-weight:bold;margin:3px;}.bad{color:white;background:red;font-weight:bold;margin:3px;}.good{color:white;background:green;font-weight:bold;margin:3px;}.hgood{color:white;background:#698705;font-weight:bold;margin:3px;}</style><div style='background:white;color:black;padding:10px;font-size:11px;font-family:Arial,sans'>"..damagelog.."<br/><br/>\n*** Damage log end.</div>"
            print('Sending the damagelog to '..ply:Nick())
            net.Start("admin_getDamageLog")
            --local dataTable = {data = damagelog};
            -- local vonData = von.serialize(dataTable);
            local data = util.Compress(damagelog)
            net.WriteUInt(#data, 32);
            net.WriteData( data, #data )
            net.Send(ply)
        end)


    hook.Add( "TTTBodyFound", "gb_dlogs_identified", function(ply, deadply, rag)
        if IsValid(deadply) then
            if SERVER and GetConVarString("gamemode") == "terrortown"  and isfunction(DamageLog) and isfunction(deadply.Nick)then
                DamageLog("ID: " .. ply:Nick() .. " identified the body of "..deadply:Nick())
            end
        end
    end )

        -- Hook for damagelogs
        hook.Add("addToDamageLog","gb_dlogs_adddamagelost",function(message)
            DamageLog( message )
        end)

        hook.Add("TTTKarmaLow", "ttt_checkkarmaban",function(ply)
            DamageLog(Format("%s was auto-banned or low karma",ply:Nick()))
        end)

    --Disconnect
    local function PlayerDisconnect( ply )
        colour3 = ply:Team()
        spawn3 = ply:Nick()

        if SERVER and GetConVarString("gamemode") == "terrortown"  and isfunction(DamageLog)then
            DamageLog("Leave: " .. spawn3 .. " ["..ply:SteamID().."] has left the server ")
        end
    end

    hook.Add( "PlayerDisconnected", "playerDisconnecteddlogs", PlayerDisconnect )

end


if CLIENT then

    local dlogsHtml = {}
    local dlogFrame = {}

    net.Receive( "admin_getDamageLog", function()
        print('receiving damagelogs')
        local leng =  net.ReadUInt(32);
        local data = net.ReadData(leng)
        data =  util.Decompress(data)
        if IsValid(dlogsHtml) then
            dlogsHtml:SetHTML( leng.." "..data )
        else
            print("Dlogs windows is closed")
        end
    end)


    local function gb_print_damagelog( ply)
        if (not IsValid(ply)) then return end
        if gb.is_jrmod(ply) or GetRoundState() != ROUND_ACTIVE then



        dlogFrame = vgui.Create("DFrame")
        dlogFrame:SetPos(200, 100)
        dlogFrame:SetSize(790, 600)
        dlogFrame:SetTitle("TTT-FUN Damage Logs viewer")
        dlogFrame:SetVisible(true)
        dlogFrame:SetDraggable(true)
        dlogFrame:ShowCloseButton(true)
        dlogFrame:SetScreenLock(true)
        dlogFrame:Center()
        dlogFrame:SetBackgroundBlur( true )
        dlogFrame.Paint = function()
            draw.RoundedBox( 8, 0, 0, dlogFrame:GetWide(), dlogFrame:GetTall(), Color( 0, 0, 0, 252 ) )
        end
        dlogFrame:MakePopup()

        dlogsHtml = vgui.Create( "HTML", dlogFrame )
        dlogsHtml:SetPos( 20, 20 )
        dlogsHtml:SetSize( 750, 560 )
        --dlogsHtml:SetHTML( "<div style='color:white'>Loading logs....</div>" )



        net.Start("admin_getDamageLogFromServer")
        net.WriteEntity( ply ) --Dummy thing
        net.SendToServer()

        end
    end
    concommand.Add("gb_print_damagelog", gb_print_damagelog)

end


