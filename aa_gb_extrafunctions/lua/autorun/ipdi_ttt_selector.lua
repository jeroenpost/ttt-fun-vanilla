
hook.Add("Initialize", "Initialize_RoleSelects", function()
     old_SelectRoles = SelectRoles
     print("Loaded RoleSelect Override");
    function SelectRoles()
        ipdi_ttt_select_roles()
    end
end)


local ipdi_tttselect = {}
ipdi_tttselect_t_debug = 0  -- [DONT CHANGE!!!] used when everyone has a t-block

--    Used PData Vars  &  Information       ----------------------------------------
-- IPDIagaintafter = Counter how many rounds player waited for T
-- IPDIagaindafter = Counter how many rounds player waited for D
-- IPDIforceTnr = If set to 1, player will be forced to Traitor, perfect for a force cmd~
-- IPDIforceDnr = If set to 1, player will be forced to Detective, perfect for a force cmd~
-- IPDIforceInr = If set to 1, player will be forced to Innocent (stackable for more rounds)

-- ipdi_t_userchance is used for set the chance for user's group which is not found in the prorioty tables.
-- Setting a group in a Table (example: ["superadmin"] = "83" ) gives the group a 83% to become the role
-- % Chances doesent effect each other for a possible fair play


--    Config     ------------------------------------
ipdi_randomselection = 0   -- (1 = Double Selection, 0 Single Selection) First random select a player, then make the prct check. 0 = Go from player to player 1, 2, 3, 4, etc. and make only the prct check.
ipdi_adminsys = "ULX" -- Type >> ULX << for ULX ADMIN SYSTEM or >> EVO << for EVOVLE ADMIN SYSTEM!!
ipdi_chose_slayed = 1 -- (1 = Also chose "Next Round Slayed" player, 0 = ignore them, dont chose) If someone is marked a "SLAY" he wont get in the chose list

ipdi_t_againafter = 0   -- How many Rounds a Player have to wait to get again choosen as a Traitor
ipdi_t_userchance = 80   -- User have a ( 80/20 ) chance to get traitor if not in a priority group
ipdi_t_allowovermax = 1  -- Allow the script to set more then MAX_Traitor's with the Forced T's | 0 set the into regurlar slots

ipdi_d_againafter = 3   -- How many Rounds a Player have to wait to get again choosen as a detective
ipdi_d_userchance = 50   -- User have a ( 50/50 ) chance to get detective if not in a priority group
ipdi_d_allowovermax = 1  -- Allow the script to set more then MAX_Detective's with the Forced D's | 0 set the into regurlar slots

-- Insert here your groups. Based for ULX
ipdi_tttselect.priority_t = {

}

ipdi_tttselect.priority_d = {

}

--   Only change if you know what you do   ---------------------------------

-- Reuse the Count function in this this script part to port the full setrole script to this file
local function GetTraitorCount(ply_count)
   -- get number of traitors: pct of players rounded down
   local traitor_count = math.floor(ply_count * GetConVar("ttt_traitor_pct"):GetFloat())
   -- make sure there is at least 1 traitor
   traitor_count = math.Clamp(traitor_count, 1, GetConVar("ttt_traitor_max"):GetInt())

   return traitor_count
end


local function GetDetectiveCount(ply_count)
   if ply_count < GetConVar("ttt_detective_min_players"):GetInt() then return 0 end

   local d_count = math.floor(ply_count * GetConVar("ttt_detective_pct"):GetFloat())
   -- limit to a max
   d_count = math.Clamp(d_count, 1, GetConVar("ttt_detective_max"):GetInt())

   return d_count
end

local function ipdi_ttt_clearblocks(again_pvar, ipdi_againafter)
  for k, v in pairs(player.GetAll()) do
    if v:GetPData( again_pvar, 0) ~= nil then
      if tonumber(v:GetPData( again_pvar, 0)) > ipdi_againafter then
        v:SetPData( again_pvar, 0)
      end
    else
      v:SetPData( again_pvar, 0)
    end
    
    if v:GetPData( again_pvar, 0) ~= nil then
      if tonumber(v:GetPData( again_pvar, 0)) >= 1 then
        v:SetPData( again_pvar,  v:GetPData( again_pvar,  0) + 1)
      end
    end
  end
end

local function ipdi_ttt_forcedt(ply)
  if ply:GetPData( "IPDIforceTnr",  0) ~= nil then
    if tonumber(ply:GetPData( "IPDIforceTnr",  0)) > 0 then
      return 1
    end
  end
  
  return 0
end

local function ipdi_ttt_forcedd(ply)
  if ply:GetPData( "IPDIforceDnr",  0) ~= nil then
    if tonumber(ply:GetPData( "IPDIforceDnr",  0)) > 0 then
      return 1
    end
  end
  
  return 0
end

local function ipdi_ttt_forcedi(ply)
  local varIPDIforceInr = ply:GetPData( "IPDIforceInr" ) or 0
  if varIPDIforceInr ~= nil then
    if tonumber(varIPDIforceInr) > 0 then
      return tonumber(varIPDIforceInr)
    end
  end
  
  return 0
end

local function ipdi_ttt_set_againt(ply)
  if ply:GetPData( "IPDIagaintafter",  0) ~= nil then
    if tonumber(ply:GetPData( "IPDIagaintafter", 0)) == 0 then
      ply:SetPData( "IPDIagaintafter", 1)
    end
  else
    ply:SetPData( "IPDIagaintafter", 1)
  end
end

local function ipdi_ttt_set_againd(ply)
  if ply:GetPData( "IPDIagaindafter",  0) ~= nil then
    if tonumber(ply:GetPData( "IPDIagaindafter", 0)) == 0 then
      ply:SetPData( "IPDIagaindafter", 1)
    end
  else
    ply:SetPData( "IPDIagaindafter", 1)
  end
end

local function ipdi_ttt_forced_sys_t(ply)

    -- Priority #1  >> FORCED TO BE T
    if ipdi_ttt_forcedt(ply) >= 1 then
      ply:SetRole(ROLE_TRAITOR)
      ply:RemovePData( "IPDIforceTnr")
      ply.BoughtDOrT = true
      return 1
    end
  return 0
end


local function ipdi_ttt_forced_sys_d(ply)
    -- Priority #1  >> FORCED TO BE D
    if ipdi_ttt_forcedd(ply) == 1 then
      ply:SetRole(ROLE_DETECTIVE)
      ply:RemovePData( "IPDIforceDnr")
      ply.BoughtDOrT = true
      return 1
    end
  return 0
end

local function ipdi_getusergroup(ply)
  if (string.lower(ipdi_adminsys) == "ulx") then
    return ply:GetUserGroup()
  elseif (string.lower(ipdi_adminsys) == "evo") then
    return ply:EV_GetRank()
  end
  
  if ply:IsSuperAdmin() then 
    return "superadmin"
  elseif ply:IsAdmin() then
    return "admin"
  end
  
  return "NoGRP"
end

local function ipdi_ttt_priority_sys_t(ply)

    -- Priority #Debug-Master >> Everyone has waiting limit on him
    if ipdi_tttselect_t_debug == 1 then
      ply:SetRole(ROLE_TRAITOR)
      ipdi_ttt_set_againt(ply)
      return 1
    end
    
  if ply:GetPData( "IPDIagaintafter", 0) ~= nil then
    if tonumber(ply:GetPData( "IPDIagaintafter", 0)) == 0 then
      local custompriority = ipdi_t_userchance
      local plyrgroup = ipdi_getusergroup(ply)
      if ipdi_tttselect.priority_t[plyrgroup] ~= nil then
        custompriority = tonumber(ipdi_tttselect.priority_t[plyrgroup])
      end
      
      -- Priority #2 >> Check Group Priority
      if custompriority >= math.random(1,100) then
        ply:SetRole(ROLE_TRAITOR)
        ipdi_ttt_set_againt(ply)
        return 1
      end
    end
  end
  
  return 0
end

local function ipdi_ttt_priority_sys_d(ply)
    
  if ply:GetPData( "IPDIagaindafter", 0) ~= nil then
    if tonumber(ply:GetPData( "IPDIagaindafter", 0)) == 0 then
      local custompriority = ipdi_d_userchance
      local plyrgroup = ipdi_getusergroup(ply)
      if ipdi_tttselect.priority_d[plyrgroup] ~= nil then
        custompriority = tonumber(ipdi_tttselect.priority_d[plyrgroup])
      end
      
      -- Priority #2 >> Check Group Priority
      if custompriority >= math.random(1,100) then
        ply:SetRole(ROLE_DETECTIVE)
        ipdi_ttt_set_againd(ply)
        return 1
      end
    end
  end
  
  return 0
end

function ipdi_ttt_select_roles()

-- Let us rebuild the selectroles functions
   local choices = {} -- array for our possible player's
   
   if not GAMEMODE.LastRole then GAMEMODE.LastRole = {} end -- i dont know why, but without it, it crash sometimes?~
   

   if not specialRound.useCustomRoles then


       old_SelectRoles()


       -- Soo... we dont have any choices... or have we? No?! so DEBUG IT!.. yeah by forget all first rules..

       for k,v in pairs(player.GetAll()) do
          if IsValid(v) and (not v:IsSpec()) then
              v.BoughtDOrT = false
             ipdi_ttt_forced_sys_t(v)
             ipdi_ttt_forced_sys_d(v)
          end
       end

   elseif server_id == 13 then
           for i, ply in pairs(player.GetAll()) do
               if (i % 2) == 0 then
                   ply:SetRole( ROLE_DETECTIVE )
               else
                   ply:SetRole( ROLE_TRAITOR )
               end
           end
   else --Specialround
       print("=========================================")
       print("======= Specialround setting roles ======")
       print("=========================================")
       for k, v in pairs(player.GetAll()) do
           if v.specialroundTraitor then
               v:SetRole( ROLE_TRAITOR )

               print( v:Nick().." traitor")
           elseif v.specialroundDetective then
               v:SetRole( ROLE_DETECTIVE )
               print( v:Nick().." detective")
           else
           --Hmm, must have joined while preparing. Well, lets just make him inno
               v:SetRole( ROLE_INNOCENT )
               print( v:Nick().." innocent??")
           end
           v.specialroundTraitor = nil
           v.specialroundDetective = nil
       end
       print("=========================================")


   end



   
   GAMEMODE.LastRole = {}

   for _, ply in pairs(player.GetAll()) do
      -- initialize credit count for everyone based on their role
      ply:SetDefaultCredits()

      -- store a uid -> role map
      GAMEMODE.LastRole[ply:UniqueID()] = ply:GetRole()
   end
end