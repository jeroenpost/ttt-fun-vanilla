function restoreRunSpeed()

    timer.Simple(1, function()
        for k,v in pairs(player.GetAll()) do
            v.resetrunspeed = false
           -- v.hasRunningBoots = true //Disabled so the old running script is disabled
           -- v:SetNWInt("runspeed", 320)
           -- v:SetNWInt("walkspeed",220)
           -- v:SetJumpPower(160)
            v:SetNWFloat("w_stamina", 1)
            v.W_Sprinting = false
        end
    end)
    timer.Create("resettherunspeed", 10, 0, function()
        for k, ply in pairs(player.GetAll()) do
            if not IsValid(ply) then return end

            if ply.OldRunSpeed and ply.OldWalkSpeed then
                if ply.resetrunspeed then
                ply:SetNWInt("runspeed", ply.OldRunSpeed)
                ply:SetNWInt("walkspeed", ply.OldWalkSpeed)
                ply.OldRunSpeed = false
                ply.OldWalkSpeed = false
                ply.resetrunspeed = false
                else
                    ply.resetrunspeed = true
                end
            elseif ply:GetNWInt("runspeed",320) < 100 then
                if ply.resetrunspeed then
                    ply:SetNWInt("runspeed", 320)
                    ply:SetNWInt("walkspeed", 220)
                    ply.resetrunspeed = false
                else
                    ply.resetrunspeed = true
                end
            elseif ply:GetNWInt("runspeed",320) > 650 then
                if ply.resetrunspeed then
                    ply:SetNWInt("runspeed", 320)
                    ply:SetNWInt("walkspeed", 220)
                    ply.resetrunspeed = false
                else
                    ply.resetrunspeed = true
                end
            end
        end
    end)
end

hook.Add("TTTPrepareRound", "resetRunSpeed", restoreRunSpeed)


-- Should we enable sprinting
local advttt_sprint_enabled = CreateConVar("ttt_advttt_sprint_enabled", "1", FCVAR_ARCHIVE)
-- How fast should sprinting players move? Multiplier of normal speed
local advttt_sprint_speedmul = CreateConVar("ttt_advttt_sprint_speedmul", "1.5", FCVAR_ARCHIVE)
-- How fast should stamina deplete while sprinting
local advttt_sprint_depletion = CreateConVar("ttt_advttt_sprint_depletion", "0.9", FCVAR_ARCHIVE)
-- How fast should stamina restore while not sprinting
local advttt_sprint_restoration = CreateConVar("ttt_advttt_sprint_restoration", "0.05", FCVAR_ARCHIVE)
-- Should we use shift instead of tapping W for sprinting? Warning: shift is used for traitor voice chat
local advttt_sprint_useshift = CreateConVar("ttt_advttt_sprint_useshift", "0", FCVAR_ARCHIVE)

util.AddNetworkString("w_stamina")

hook.Add("KeyPress", "WyoziAdvTTTSprintTap", function(ply, key)
    if (key == IN_FORWARD or key == IN_SPEED) and advttt_sprint_enabled:GetBool() then
        if ((ply.W_LastWTap and ply.W_LastWTap > CurTime() - 0.5) or key == IN_SPEED) and ply:GetNWFloat("w_stamina", 0) > 0.1 then
            ply.W_Sprinting = true
        end
        ply.W_LastWTap = CurTime()
    end
end)

hook.Add("KeyRelease", "WyoziAdvTTTSprintTap", function(ply, key)
    local thekey = IN_FORWARD
    if (key == IN_FORWARD and not ply:KeyDown(IN_SPEED)) or key == IN_SPEED then
        ply.W_Sprinting = false
    end
end)

local function GetSpeedMultiplier(ply)
    local mul = 220
    local rest = not ply:IsOnGround()

    local normal_walkspeed = 220
    local normal_runspeed = 320

    local walkspeed = ply:GetNetworkedInt("walkspeed",normal_walkspeed)
    local runspeed = ply:GetNetworkedInt("runspeed",normal_runspeed)
    local stamina = ply:GetNWFloat("w_stamina", 0)
    local regen = advttt_sprint_restoration:GetFloat() * ( ply:GetNWInt("staminaboost",0) + 1)


    if ply.W_Sprinting and ply:GetNWFloat("w_stamina", 0) > 0 then
        mul = ((runspeed - normal_walkspeed) * stamina) + normal_walkspeed
        local speed = ply:GetVelocity():Length()
        if speed > walkspeed and GetRoundState() == ROUND_ACTIVE then
            ply:SetNWFloat("w_stamina", math.max(stamina-(advttt_sprint_depletion:GetFloat() * 0.003), 0))
        end
    else
        mul = walkspeed
        if not ply:KeyDown(IN_JUMP) then
            ply:SetNWFloat("w_stamina", math.min(stamina+(regen * 0.003), 1))
        end
        ply.W_Sprinting = false
    end

    return mul
end

hook.Add("WyoziGenericSpeedMul", "AdvancedTTT", GetSpeedMultiplier)


local function PMetaSetSpeed(self, slowed)
    local mul = GetSpeedMultiplier(self)

    if slowed then
        self:SetWalkSpeed(mul-100)
        self:SetRunSpeed(mul-100)
        self:SetMaxSpeed(mul-100)
    else
        self:SetWalkSpeed(mul)
        self:SetRunSpeed(mul)
        self:SetMaxSpeed(mul)
    end
end

hook.Add("InitPostEntity", "WyoziAdvTTTOverrideSetSpeed", function()
    local pmeta = FindMetaTable("Player")
    pmeta.SetSpeed = PMetaSetSpeed
end)
