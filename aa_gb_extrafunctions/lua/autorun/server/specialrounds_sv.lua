util.AddNetworkString("specialroundMessage")
--util.AddNetworkString("specialroundSetTeamHud")
util.AddNetworkString("specialroundRemoveHatsAndTrails")
specialRound.nextSpecialRound = false
specialRound.isSpecialRound = false
specialRound.forcedRound = false

if not ConVarExists("specialround_start_roundsleft") then CreateConVar("specialround_start_roundsleft", 2, { FCVAR_ARCHIVE }) end

hook.Add("TTTPrepareRound","reserstuffthatmightbebroken",function()
    RunConsoleCommand("sv_gravity", 600)
    for k, v in pairs(player.GetAll()) do
        ULib.invisible(v, false, 0)
    end
    timer.Simple(450,function()
        RunConsoleCommand("ttt_debug_preventwin", "0");
    end)
end)

specialRound.SendMessage = function(msg)
    net.Start("specialroundMessage")
    net.WriteTable({ msg, 333 })
    net.Broadcast()
end

specialRound.ShouldStart = function()
    specialRound.nextSpecialRound = false
    specialRound.useCustomRoles = false

    local pointshopround = specialRound.CheckPointshop()
    if pointshopround then
        specialRound.Announce("")
        SetGlobalInt("special_rounds_this_map", GetGlobalInt("special_rounds_this_map") + 1)
        specialRound.isSpecialRound = true
    elseif (GetConVarNumber("specialround_start_roundsleft") or 2) >= GetGlobalInt("ttt_rounds_left", 6) then
        specialRound.GetRandom()
        specialRound.Announce("")
        specialRound.isSpecialRound = true
    elseif math.random() <= 0.02 then
        specialRound.nextSpecialRound = specialRound.Modes[7]
        specialRound.Announce("")
        specialRound.isSpecialRound = true
    elseif specialRound.forcedRound and specialRound.forcedRound > 0 and  specialRound.Modes[specialRound.forcedRound ] then
        specialRound.nextSpecialRound = specialRound.Modes[specialRound.forcedRound ]
        specialRound.Announce("")
        specialRound.isSpecialRound = true
        specialRound.forcedRound = false
    else
        specialRound.isSpecialRound = false
    end


    if not specialRound.nextSpecialRound then return end
    specialRound.Announce("")
    func = specialRound.nextSpecialRound[3]
    id = specialRound.nextSpecialRound[4]
    role = specialRound.nextSpecialRound[5]
    if not id then id = false end
    if not role then role = false end

    --_G[func](id, role)

    if func == "detective_vs_traitor" then
        specialRound.detective_vs_traitor_preround(id, role)
        specialRound.useCustomRoles = true
    elseif func == "low_gravity" then
        specialRound.low_gravity_preround(id, role)
    elseif func == "bossfight" then
        specialRound.bossfight_preround(id, role)
        specialRound.useCustomRoles = true
    elseif func == "infected" then
        specialRound.infected_preround(id, role)
        specialRound.useCustomRoles = true
    elseif func == "weirdmodel" then
        -- specialRound.weirdmodel_preround( id, role )
    elseif func == "hidden" then
        specialRound.hidden_preround(id, role)
        specialRound.useCustomRoles = true
    elseif func == "bigheads" then
        specialRound.bigheads(id, role)
    elseif func == "snowballs" then
        specialRound.snowballs()
    elseif func == "murder" then
        specialRound.useCustomRoles = true
        specialRound.murder_preround()
    elseif func == "battle_royale" then
        specialRound.useCustomRoles = true
        specialRound.battle_royale_preround()
    elseif func == "c4" then
        specialRound.useCustomRoles = true
        specialRound.c4_preround()
    elseif func == "shuffle" then
        specialRound.useCustomRoles = true
        specialRound.shuffle_preround()
    end
end

hook.Add("TTTPrepareRound", "specialRoundsInit", specialRound.ShouldStart)


specialRound.GetRandom = function()
    specialRound.nextSpecialRound = table.Random(specialRound.Modes)
end

specialRound.Announce = function(message)
    mode = specialRound.nextSpecialRound[2]
    specialRound.SendMessage(message .. mode)
end



specialRound.Start = function()
    if not specialRound.nextSpecialRound then return end
    specialRound.Announce("")
    func = specialRound.nextSpecialRound[3]
    id = specialRound.nextSpecialRound[4]
    role = specialRound.nextSpecialRound[5]
    if not id then id = false end
    if not role then role = false end

    --_G[func](id, role)

    if func == "detective_vs_traitor" then
        specialRound.detective_vs_traitor(id, role)
    elseif func == "low_gravity" then
        specialRound.low_gravity(id, role)
    elseif func == "bossfight" then
        specialRound.bossfight(id, role)
    elseif func == "infected" then
        specialRound.infected(id, role)
    elseif func == "weirdmodel" then
        specialRound.weirdmodel(id, role)
    elseif func == "hidden" then
        specialRound.hidden(id, role)
    elseif func == "bigheads" then
         specialRound.bigheads( id, role )
    elseif func == "snowballs" then
        specialRound.snowballs( )
    elseif func == "murder" then
        specialRound.murder( )
    elseif func == "battle_royale" then
        specialRound.battle_royale()
    elseif func == "c4" then
        specialRound.useCustomRoles = true
        specialRound.c4()
    elseif func == "shuffle" then
        specialRound.useCustomRoles = true
        specialRound.shuffle()
    end
end


hook.Add("TTTBeginRound", "specialRound.Start", specialRound.Start)


specialRound.ResetGravity = function()
    RunConsoleCommand("sv_gravity", 600)
end


specialRound.CheckPointshop = function()
    for k, v in pairs(player.GetAll()) do

        if v.GotBossRoundD then
            v.GotBossRoundD = nil
            v.HadBossRoundMap = true
            specialRound.nextSpecialRound = { "", "KOS " .. v:Nick(), "bossfight", v:UniqueID(), "detective" }
            return true
        end

        if v.GotBossRoundT then
            v.GotBossRoundT = nil
            v.HadBossRoundMap = true
            specialRound.nextSpecialRound = { "", "KOS " .. v:Nick(), "bossfight", v:UniqueID(), "traitor" }
            return true
        end

        if v.GotTheHidden then
            v.GotTheHidden = nil
            v.HadBossRoundMap = true
            specialRound.nextSpecialRound = { "", "" .. v:Nick() .. " is The Hidden", "hidden", v:UniqueID() }
            return true
        end

        if v.GotTheInfected then
            v.GotTheInfected = nil
            v.HadBossRoundMap = true
            specialRound.nextSpecialRound = { "", "" .. v:Nick() .. " is The Infected", "infected", v:UniqueID() }
            return true
        end
    end
    return false
end


-- Detective VS Traitor --------------------------------------------------------
specialRound.detective_vs_traitor_preround = function()
    local startRole = "traitor"
    for k, v in pairs(player.GetAll()) do
        v.drawHalo = true
        if startRole ~= "traitor" then
            v.specialroundTraitor = false
            v.specialroundDetective = true
            v:RemovePData("IPDIforceDnr")
            startRole = "traitor"
        else
            v.specialroundTraitor = true
            v.specialroundDetective = false
            v:SetPData("IPDIforceDnr", 1)
            startRole = "detective"
        end
    end

end

specialRound.detective_vs_traitor = function()
   -- specialRound.SetTeamHud(true)
end

-- Low Gravity -----------------------------------------------------------------
specialRound.low_gravity_preround = function()
    RunConsoleCommand("sv_gravity", 120)
    hook.Add("TTTEndRound", "specialroundresetGravity2", function()
        specialRound.ResetGravity()
    end)
end
specialRound.low_gravity = function()

end

-- BossFight -------------------------------------------------------------------
specialRound.bossfight_preround = function(plyID, role)
    role1 = "D"
    role2 = "T"
    randomply = nil

    if plyID then

        randomply = player.GetByUniqueID(plyID)

        if role == "detective" then
            role2 = "D"
            role1 = "T"
        else
            role1 = "D"
            role2 = "T"
        end
    end
    if not randomply or not IsValid(randomply) then
        randomply = table.Random(player.GetAll())
    end

    local numberOfPlayers = table.Count(player.GetAll())
    local bossHealth = numberOfPlayers * 350
    v = randomply
    if role2 == "D" then
        v.specialroundTraitor = false
        v.specialroundDetective = true
    else
        v.specialroundTraitor = true
        v.specialroundDetective = false
    end


    for k, v in pairs(player.GetAll()) do
        if v ~= randomply then
            if role1 == "D" then
                v.specialroundTraitor = false
                v.specialroundDetective = true
            else
                v.specialroundTraitor = true
                v.specialroundDetective = false
            end
        end
    end
end
specialRound.bossfight = function(plyID, role)

    --specialRound.SetTeamHud(true)

    if role == "detective" then
        role2 = ROLE_DETECTIVE
        role1 = ROLE_TRAITOR
    else
        role1 = ROLE_DETECTIVE
        role2 = ROLE_TRAITOR
    end


    local numberOfPlayers = table.Count(player.GetAll())
    local bossHealth = numberOfPlayers * 350

    for k, v in pairs(player.GetAll()) do

        if v:GetRole() == role2 then
            v:SetCredits(15)
            v:SetHealth(bossHealth);
            v:PrintMessage(HUD_PRINTTALK, "BOSSFIGHT: You are the BOSS, Kill them ALL")
            v.OldScaleSpecial = v:GetModelScale()
            v:GiveEquipmentItem(EQUIP_RADAR)
            v:SetModelScale(2.5, 1)
            v.IsSpecialPerson = true
            _globals['specialperson'][v:SteamID()] = true
        else
            v:SetCredits(3)
            v:PrintMessage(HUD_PRINTTALK, "BOSSFIGHT: Work together to kill " .. randomply:Nick() .. " with " .. bossHealth .. " health")
        end
    end
    hook.Add("TTTEndRound", "Ps_resetBoosss", function() timer.Simple(0.5, specialRound.bossfight_postround) end)
end


function specialRound.bossfight_postround()
    for k, v in pairs(player.GetAll()) do
        --reset boss status
        v.PS_EquippedThisRound = {}
        v.BoughtBossThisRound = false
        v.IsSpecialPerson = false
        v:SetColor(COLOR_WHITE)
        v:SetModelScale(1, 0.02)
        _globals['specialperson'][v:SteamID()] = false


        if (v.OldScaleSpecial) then
            v:SetModelScale(v.OldScaleSpecial, 0.05)
            v.OldScaleSpecial = false
        end
    end
end

-- The Hidden ------------------------------------------------------------------
specialRound.hidden_preround = function(plyID)
    randomply = nil
    if plyID then
        randomply = player.GetByUniqueID(plyID)
    end

    if not randomply or not IsValid(randomply)  then
        randomply = table.Random(player.GetAll())
        local c = 0
        while randomply:IsSpec() do
            print("Ply was a spectator")
            randomply = table.Random(player.GetAll())
            c = c + 1
            if c > 200 then return end
        end
    end
    print("=========================================")
    print( "Hidden Preround running")
    print( randomply:Nick().." Will be the hidden")
    print("=========================================")

    randomply.specialroundTraitor = true
    randomply.specialroundDetective = false



    for k, v in pairs(player.GetAll()) do
        v.BoughtBossThisRound = false
        if v ~= randomply then
            v.specialroundTraitor = false
            v.specialroundDetective = true
        end
    end
end
specialRound.hidden = function(plyID)

    local plyCountTraii = table.Count(player.GetAll())

    for k, v in pairs(player.GetAll()) do

        if v:GetRole() ~= ROLE_TRAITOR then

            v:SetCredits(5)
            v:PrintMessage(HUD_PRINTTALK, "THE HIDDEN: Work together to kill the hidden traitor")

        else

            v:SetCredits(2)

            v:SetHealth(100 + (plyCountTraii * 10));
            v:StripWeapons()
            v:Give("weapon_hidden_knife")
            v.CannotPickupWeapons = true
            v:PrintMessage(HUD_PRINTTALK, "THE HIDDEN: You are the HIDDEN, Kill them ALL with your knife while being invisible")
            ULib.invisible(v, true, 90)
            v:GiveEquipmentItem(EQUIP_RADAR)

            timer.Simple(1, function() v:ConCommand("ttt_radar_scan") end)
            v.IsSpecialPerson = true
            _globals['specialperson'][v:SteamID()] = true
        end
    end
    hook.Add("Think", "infectedradar", function()
        if deathMatchNextThink < CurTime() and GetRoundState() == ROUND_ACTIVE then
            deathMatchRadarScan = CurTime() + 1
            for k, v in pairs(player.GetAll()) do
                if v:Alive() and v:GetRole() == ROLE_TRAITOR then
                    if not v:HasEquipmentItem(EQUIP_RADAR) then
                        v:GiveEquipmentItem(EQUIP_RADAR)
                    end
                    v.radar_charge = 0
                    if v:HasEquipmentItem(EQUIP_RADAR) then
                        v:ConCommand("ttt_radar_scan")
                    end
                    v.radar_charge = 1
                end
            end
        end
    end)

    hook.Add("TTTEndRound", "Uncloak everyone", function()
        hook.Remove("Think", "infectedradar")
        for k, v in pairs(player.GetAll()) do
            ULib.invisible(v, false, 0)
            _globals['specialperson'][v:SteamID()] = false

        end
    end)
end

-- The Infected ----------------------------------------------------------------
specialRound.infected_preround = function(plyID)
    randomply = nil
    if plyID then

        randomply = player.GetByUniqueID(plyID)
    end

    if not randomply or not IsValid(randomply) or not randomply:IsPlayer() then
        randomply = table.Random(player.GetAll())
        local c = 0
        while randomply:IsSpec() do
            print("Ply was a spectator")
            randomply = table.Random(player.GetAll())
            c = c + 1
            if c > 200 then return end
        end
    end
    randomply.specialroundTraitor = true
    randomply.specialroundDetective = false


    for k, v in pairs(player.GetAll()) do
        v.BoughtBossThisRound = false
        if v ~= randomply then
            v.specialroundTraitor = false
            v.specialroundDetective = true
        end
    end
end
specialRound.infected = function(plyID)


    local plyCountTraii = table.Count(player.GetAll())

    for k, v in pairs(player.GetAll()) do

        if v:GetRole() ~= ROLE_TRAITOR then
            v:PrintMessage(HUD_PRINTTALK, "THE INFECTED: Work together to kill the infected traitor ")

        else

            v:SetHealth(300 + (plyCountTraii * 10));
            v:StripWeapons()
            v:Give("weapon_infected_claws")
            v.hasProtectionSuit = true
            v:PrintMessage(HUD_PRINTTALK, "THE INFECTED: You are the INFECTED, Infect them ALL with your claws")
            v:SetModel("models/player/zombie_classic.mdl")
            v:SetModelScale(1, 0.02)
            v.IsSpecialPerson = true

            _globals['specialperson'][v:SteamID()] = true
            v:GiveEquipmentItem(EQUIP_RADAR)
            timer.Simple(1, function() v:ConCommand("ttt_radar_scan") end)
            v.CannotPickupWeapons = true
            timer.Create("specialroundSetPlayermodel" .. v:SteamID(), 0.25, 20, function()
                if not IsValid(v) then return end
                v:SetModel("models/player/zombie_classic.mdl")
                v:SetModelScale(1, 0.02)
            end)
        end
    end

    hook.Add("Think", "infectedradar", function()
        if deathMatchNextThink < CurTime() and GetRoundState() == ROUND_ACTIVE then
            deathMatchRadarScan = CurTime() + 1
            for k, v in pairs(player.GetAll()) do
                if v:Alive() and v:GetRole() == ROLE_TRAITOR then
                    if not v:HasEquipmentItem(EQUIP_RADAR) then
                        v:GiveEquipmentItem(EQUIP_RADAR)
                    end
                    v.radar_charge = 0
                    if v:HasEquipmentItem(EQUIP_RADAR) then
                        v:ConCommand("ttt_radar_scan")
                    end
                    v.radar_charge = 1
                end
            end
        end
    end)

    hook.Add("TTTEndRound", "specialroundsResetStatus", function()
        hook.Remove("Think", "infectedradar")
        for k, v in pairs(player.GetAll()) do

            _globals['specialperson'][v:SteamID()] = false

        end
    end)
end



-- Weird Playermodel -----------------------------------------------------------

specialRound.weirdmodel = function()
    local modelsss = { "models/props_c17/oildrum001.mdl", "models/AntLion.mdl", "models/antlion_guard.mdl" }
    local randmodel = table.Random(modelsss)

    for k, v in pairs(player.GetAll()) do
        v:SetModel(randmodel)
        v:SetModelScale(1, 0.02)
        timer.Create("specialroundSetPlayermodel" .. v:SteamID(), 0.25, 20, function()
            if not IsValid(v) then return end
            v:SetModel(randmodel)
            v:SetModelScale(1, 0.02)
        end)
    end
end

-- BigHeads -----------------------------------------------------------

specialRound.bigheads = function()
    for _, ply in pairs(player.GetAll()) do
        timer.Simple(3.05,function()
            if IsValid(ply) and ply:Alive() and ply:IsTerror() then
            local boneid = ply:LookupBone("ValveBiped.Bip01_Head1")
            if boneid then
                ply:ManipulateBoneScale(boneid, Vector(3, 3, 3))
            end
            end
        end)
    end

    -- Reset playerbody when specialround


    hook.Add("TTTEndRound", "pointshopresetHeads", function()
        for _, ply in pairs(player.GetAll()) do
            local boneid = ply:LookupBone("ValveBiped.Bip01_Head1")
            if boneid then
                ply:ManipulateBoneScale(boneid, Vector(1, 1, 1))
            end
        end
    end)
end

-- Snowballs -----------------------------------------------------------

specialRound.snowballs = function()
    local ClassName
    for k,v in pairs( weapons.GetList() ) do
        if v.ClassName then ClassName = v.ClassName else ClassName = "def" end
        if ClassName ~= "custom_hanks_balls" and ClassName ~= "snowball_thrower" then
            for k2,v2 in pairs( ents.FindByClass( ClassName )) do
                v2:Remove()
            end
        end
    end

    for k, v in pairs( player.GetAll() ) do
        if v:Team() == TEAM_TERROR then
            v:StripWeapons()
            v:Give("custom_hanks_balls")
            v:Give("snowball_thrower")
            v:SelectWeapon("custom_hanks_balls")
        end
    end

end

-- Murder -------------------------------------------------------------------
specialRound.murder_preround = function(plyID, role)
    role1 = "D"
    role2 = "T"
    randomply = nil

    if plyID then

        randomply = player.GetByUniqueID(plyID)

            role1 = "D"
            role2 = "T"

    end
    if not randomply or not IsValid(randomply) then
        randomply = table.Random(player.GetAll())
    end

    local numberOfPlayers = table.Count(player.GetAll())
    local bossHealth = numberOfPlayers * 350
    v = randomply

        v.specialroundTraitor = true
        v.specialroundDetective = false


    for k, v in pairs(player.GetAll()) do
        if v ~= randomply then

                v.specialroundTraitor = false
                v.specialroundDetective = false
                v.specialroundInnocent = true

        end
    end
end
specialRound.murder = function(plyID, role)

        role1 = ROLE_DETECTIVE
        role2 = ROLE_TRAITOR

    local numberOfPlayers = table.Count(player.GetAll())
    local bossHealth = numberOfPlayers * 25

    for k, v in pairs(player.GetAll()) do

        if v:GetRole() == role2 then
            v:SetCredits(25)
            v:SetHealth(bossHealth + 100);
            v:PrintMessage(HUD_PRINTTALK, "BOSSFIGHT: You are the BOSS, Kill them ALL")
            v.OldScaleSpecial = v:GetModelScale()
            v:GiveEquipmentItem(EQUIP_RADAR)
            v.IsSpecialPerson = true
            _globals['specialperson'][v:SteamID()] = true
        end
    end
    hook.Add("TTTEndRound", "Ps_resetBoosss", function() timer.Simple(0.5, specialRound.murder_postround) end)
end


function specialRound.murder_postround()
    for k, v in pairs(player.GetAll()) do
        --reset boss status
        v.PS_EquippedThisRound = {}
        v.BoughtBossThisRound = false
        v.IsSpecialPerson = false
       -- v:SetColor(COLOR_WHITE)
        v:SetModelScale(1, 0.02)
        _globals['specialperson'][v:SteamID()] = false


        if (v.OldScaleSpecial) then
            v:SetModelScale(v.OldScaleSpecial, 0.05)
            v.OldScaleSpecial = false
        end
    end
end


--- BATTLE ROYALE
specialRound.battle_royale_randomWeapon = "black_green"
specialRound.battle_royale_preround = function()

    specialRound.battle_royale_randomWeapons = { "custom_hanks_balls","black_green","gb_nanners","gb_trashgun","gb_handgun", "weapon_ttt_m16crowbar","weapon_ttt_mp7", "weapon_nyangun", "gb_golden_deagle","doom3_chainsaw", "gb_bow","weapon_zm_sledge"}
    specialRound.battle_royale_randomWeapons = { "weapon_ttt_sipistol","weapon_ttt_famas", "gb_golden_deagle","weapon_ttt_m16crowbar","weapon_zm_sledge"}
    specialRound.battle_royale_randomWeapon = table.Random( specialRound.battle_royale_randomWeapons)

    RunConsoleCommand("ttt_debug_preventwin", "1");


    for k, v in pairs(player.GetAll()) do
        v.specialroundDetective = true
    end

end
specialRound.battle_royale = function()
    RunConsoleCommand("ttt_debug_preventwin", "1");

    local ClassName
    for k,v in pairs( weapons.GetList() ) do

        for k2,v2 in pairs( ents.FindByClass( v.ClassName or 0 )) do
            -- if IsValid(ent2) then
            local pos = v2:GetPos()
            local ang = v2:GetAngles()
            v2:Remove()

            local ent2 = ents.Create( specialRound.battle_royale_randomWeapon )
            pos.z = pos.z + 5
            ent2:SetPos(pos)
            ent2:SetAngles( ang )
            ent2:Spawn()
            ent2:PhysWake()
        end
    end


    for k, v in pairs(player.GetAll()) do
        if v:Team() == TEAM_TERROR then
            v:StripWeapons()
            v:Give(specialRound.battle_royale_randomWeapon)
            v:SelectWeapon(specialRound.battle_royale_randomWeapon)
            v:SetCredits(-100)
            v:GiveEquipmentItem(EQUIP_RADAR)
            v:SetHealth(125);
            v:PrintMessage(HUD_PRINTTALK, "Battle Royale, kill all with the same weapon all, be the only survivor and win 10000 points")
        end
    end

    timer.Create("Ps_BattleRoyale_think",3,0, function()
        local numalive = 0
        local aliveperson = ""
        local alivePersonPly = false
        for k, v in pairs(player.GetAll()) do
            if v:Team() == TEAM_TERROR and v:Alive() then
                numalive = numalive + 1
                aliveperson = v:Nick();
                alivePersonPly = v
                v.radar_charge = 0
                -- Sometimes doesnt work so check first
                if v:HasEquipmentItem(EQUIP_RADAR) then
                    v:ConCommand("ttt_radar_scan")
                else
                    v:GiveEquipmentItem(EQUIP_RADAR)
                end
            end
        end

        if numalive == 1 then
            specialRound.Announce(aliveperson.." WINS")
            if alivePersonPly then
                alivePersonPly:PS_GivePoints(10000)
            end

            timer.Simple(3,function()
                RunConsoleCommand("ttt_debug_preventwin", "0");
                timer.Stop("Ps_BattleRoyale_think");
                --EndRound()
            end)
        end
        if numalive < 1 then
            RunConsoleCommand("ttt_debug_preventwin", "0");
            timer.Stop("Ps_BattleRoyale_think");
        end

    end)

    hook.Add("TTTEndRound", "Ps_resetBattleRoyale", function() timer.Simple(0.5, specialRound.battle_royale_postround ) end)

end
specialRound.battle_royale_postround = function()
    RunConsoleCommand("ttt_debug_preventwin", "0");
    timer.Destroy("Ps_BattleRoyale_think");
end




-- Disarm the C4
specialRound.c4_preround = function()

    RunConsoleCommand("ttt_debug_preventwin", "1");
    specialRound.c4_endtime = 120

    for k, v in pairs(player.GetAll()) do
        v.specialroundTraitor = true
        v.CannotPickupWeapons = true
    end

end

specialRound.c4 = function()
    RunConsoleCommand("ttt_debug_preventwin", "1");
    specialRound.c4_endtime = 100

    for k, v in pairs(ents.GetAll()) do if v:GetClass() == "prop_physics" then v:Remove() end end

    local ClassName
    for k,v in pairs( weapons.GetList() ) do
        for k2,v2 in pairs( ents.FindByClass(v.ClassName or 0 )) do

            local ent2 = ents.Create( "ttt_c4" )
            if IsValid(ent2) then
                local pos = v2:GetPos()
                local ang = v2:GetAngles()
                v2:Remove()
                local explodetime = math.random(60,300)
                pos.z = pos.z + 3
                ent2:SetPos(pos)
                ent2:SetAngles( ang )
                ent2:SetDetonateTimer(explodetime)
                ent2:Spawn()
                ent2:PhysWake()
                ent2:SetDetonateTimer(explodetime)
                ent2:WeldToGround(true)
                ent2.DisarmCausedExplosion = false
                ent2.SafeWires = {}

                local choices = {}
                for i=1, 6 do
                    table.insert(choices, i)
                end

                -- random selection process, lot like traitor selection
                local safe_count = ent2.SafeWiresForTime(time)
                local safes = {}
                local picked = 0
                while picked < safe_count do
                    local pick = math.random(1, #choices)
                    local w = choices[pick]

                    if not ent2.SafeWires[w] then
                        ent2.SafeWires[w] = true
                        table.remove(choices, pick)

                        picked = picked + 1
                    end
                end

                -- send indicator to traitors
                ent2:SetArmed(true)
                ent2:SendWarn(true)
            end

        end
    end

    for k, v in pairs(player.GetAll()) do
        if v:Team() == TEAM_TERROR then
            v:StripWeapons()
            v.CannotPickupWeapons = false
            v:Give("weapon_ttt_c4")
            v:SetCredits(-100)
            v.numc4disarms = 0
            v:PrintMessage(HUD_PRINTTALK, "Disarm the C4's. The one with the most disarms wins 10000 points")
        end
    end

    timer.Create("Ps_c4_end_functionChaech",120,1, specialRound.c4_endfunction )

    timer.Create("Ps_c4_think",5,0, function()
        local numalive = 0
        specialRound.c4_endtime = specialRound.c4_endtime - 5

        for k, v in pairs(player.GetAll()) do
            if v:Team() == TEAM_TERROR and v:Alive() then
                numalive = numalive + 1
                v:Give("weapon_ttt_c4")

                if specialRound.c4_endtime > 6 then
                 v:PrintMessage(HUD_PRINTTALK, specialRound.c4_endtime.." Seconds left")
                else
                    specialRound.c4_endfunction()
                    specialRound.c4_postround()
                end
            end
        end

        if numalive < 2 then
            specialRound.c4_endfunction()
            specialRound.c4_postround()
        end

    end)

    hook.Add("TTTEndRound", "Ps_resetc4", function() timer.Simple(0.5, function() specialRound.c4_postround() end ) end)

end
specialRound.c4_endfunction = function()
    local bestGuy = "Nobody"
    local bestGuyPly = false
    local bestDisarms = 0

    for k, v in pairs(player.GetAll()) do
        if v.numc4disarms and v.numc4disarms > bestDisarms then
            bestDisarms = v.numc4disarms
            bestGuy = v:Nick()
            bestGuyPly = v
        end
    end
    bestGuy = "";
    for k, v in pairs(player.GetAll()) do
        if v.numc4disarms and v.numc4disarms == bestDisarms then
            bestGuy = bestGuy.." \n"..v:Nick()
            v:PS_GivePoints(10000)
        end
    end

    for k, v in pairs(player.GetAll()) do
        v:PrintMessage( HUD_PRINTCENTER, bestGuy.." \nWINS with "..bestDisarms.." disarms" )
    end


    timer.Simple(3,function()
        RunConsoleCommand("ttt_debug_preventwin", "0");
        timer.Destroy("Ps_c4_think");
        --EndRound()
    end)

end
specialRound.c4_postround = function()
    RunConsoleCommand("ttt_debug_preventwin", "0");
    timer.Destroy("Ps_c4_think");
    hook.Remove("TTTEndRound", "Ps_resetc4")
end


-- Shuffle

local function OppositeRole(role)
    return role == ROLE_TRAITOR and ROLE_DETECTIVE or ROLE_TRAITOR
end

specialRound.shuffle_preround = function()

    local startrank = table.Random({"d","t"})
    for k, v in pairs(player.GetAll()) do
        if (k % 2) == 0 then
            v.specialroundTraitor = true
            v.specialroundDetective = false
        else
            v.specialroundDetective = true
            v.specialroundTraitor = false
        end

        v:PrintMessage(HUD_PRINTTALK, "1. 50% Traitors, 50% Detectives")
        v:PrintMessage(HUD_PRINTTALK, "2. Kill members of the other role")
        v:PrintMessage(HUD_PRINTTALK, "3. Killing someone converts them to your role")
        v:PrintMessage(HUD_PRINTTALK, "4. Everyone has three lives!")
        v.ShuffleLives = 3
    end

    local wepoptions = {
        "weapon_zm_shotgun",
        "weapon_zm_mac10",
        "weapon_ttt_m16",
        "weapon_zm_sledge"
    }

    hook.Add("PlayerDeath","specialround_shuffle",function(victim, weapon, killer)
        if IsValid(killer) and killer:IsPlayer() then
            killer:AddCredits(1)
        end

        local myrole = victim:GetRole()

        victim.ShuffleLives = (victim.ShuffleLives or 1) - 1
        if victim.ShuffleLives > 0 then
            timer.Simple(0, function()
                if IsValid(victim.server_ragdoll) then
                    victim.server_ragdoll:Remove()
                end

                victim:UnSpectate()
                victim:SetTeam(TEAM_TERROR)
                victim:StripAll()
                victim:Spawn()
                victim:SetMoveType(MOVETYPE_WALK)

                local newrole
                if IsValid(killer) and killer:IsPlayer() then
                    newrole = (myrole == killer:GetRole()) and OppositeRole(myrole) or killer:GetRole()
                else
                    newrole = OppositeRole(myrole)
                end
                victim:SetRole(newrole)
                victim:Give(table.Random(wepoptions))

                local livlev = "You have " .. tostring(victim.ShuffleLives) .. " lives left."

                victim:ChatPrint(livlev)

                SendFullStateUpdate()
            end)
        end
    end)

     hook.Add("TTTEndRound","Shuffle_endround",specialRound.shuffleRoundend)

end

specialRound.shuffleRoundend = function()
   -- specialRound.SetTeamHud(true)
    hook.Remove("PlayerDeath","specialround_shuffle")
    hook.Remove("TTTEndRound","Shuffle_endround")
end