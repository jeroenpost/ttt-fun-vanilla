hook.Add("Initialize","LoadTheAdverts", function()
    if not ulx.addAdvert then
        return
    end

    http.Fetch( "https://ttt-fun.com/adverts/"..server_id,
        function( body, len, headers, code )
            local data_root, err = ULib.parseKeyValues( ULib.stripComments( body , ";" ) )
            if not data_root then Msg( "[ULX] Error in advert config: " .. err .. "\n" ) return end

            for group_name, row in pairs( data_root ) do
                if type( group_name ) == "number" then -- Must not be a group
                    local color = Color( tonumber( row.red ) or ULib.DEFAULT_TSAY_COLOR.r, tonumber( row.green ) or ULib.DEFAULT_TSAY_COLOR.g, tonumber( row.blue ) or ULib.DEFAULT_TSAY_COLOR.b )
                    ulx.addAdvert( row.text or "NO TEXT SUPPLIED FOR THIS ADVERT", tonumber( row.time ) or 300, _, color, tonumber( row.time_on_screen ) )
                else -- Must be a group
                    if type( row ) ~= "table" then Msg( "[ULX] Error in advert config: Adverts are not properly formatted!\n" ) return end
                    for i=1, #row do
                        local row2 = row[ i ]
                        local color = Color( tonumber( row2.red ) or 151, tonumber( row2.green ) or 211, tonumber( row2.blue ) or 255 )
                        ulx.addAdvert( row2.text or "NO TEXT SUPPLIED FOR THIS ADVERT", tonumber( row2.time ) or 300, group_name, color, tonumber( row2.time_on_screen ) )
                    end
                end
            end
        end,
        function( error )
        -- We failed. =(
        end
    );


end)