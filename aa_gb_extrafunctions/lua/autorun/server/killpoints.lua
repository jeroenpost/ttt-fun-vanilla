local RolePoints = {
    [2] = {
        [2] = -2500, --Killed Detective as Detective
        [0] = -50, --Killed Innocent as Detective
        [1] = 500 --Killed Traitor as Detective
    },
    [0] = {
        [2] = -2500, --Killed Detective as Innocent
        [0] = -50, --Killed Innocent as Innocent
        [1] = 250 --Killed Traitor as Innocent
    },
    [1] = {
        [2] = 100, --Killed Detective as Traitor
        [0] = 100, --Killed Innocent as Traitor
        [1] = -2500 --Killed Traitor as Traitor
    }
}

function NotifyPlayer(ply, killer, dmginfo)

    if  not ply:IsBot() and not ply:IsGhost() or (specialround and specialRound.isSpecialRound != true) then
    local num = RolePoints[killer:GetRole()][ply:GetRole()]
    local bool = string.find(num, "-")
    local dontTakePoints = false

    if server_id == 13 then
        dontTakePoints = true
    end
    local weapon = killer:GetActiveWeapon()
    local shotWith = ""
    if IsValid(weapon) then
        if weapon:GetTable().PrintName  ~= nil then
            shotWith = " with a(n) " .. weapon:GetTable().PrintName
        end
    end

    if dmginfo:IsExplosionDamage() then
        shotWith = " by an explosion"
        dontTakePoints = true
    end

    if server_id == 13 then
        num = num/5

    end

    ply:ChatPrint("[KILLLOG] You were killed by " .. killer:Nick() .. " (" .. killer:GetRoleString() .. ")" .. shotWith .. ".")
    if bool then
        if not dontTakePoints and server_id ~= 13 then
            killer:ChatPrint("[KILLPOINTS] " .. (num * -1) .. " points taken away from you for killing " .. ply:Nick() .. " (" .. ply:GetRoleString() .. ") !")
        end
        if server_id == 13 then
            ply:PS_GivePoints(50)
            killer:ChatPrint("[KILLPOINTS] You received 50 points for killing " .. ply:Nick() .. " (" .. ply:GetRoleString() .. ")")
        else
            ply:PS_GivePoints(1500)
        end
        ply:ChatPrint("[KILLPOINTS] You received 1500 points: " .. killer:Nick() .. " (" .. killer:GetRoleString() .. ") killed you as buddy")
    else
        killer:ChatPrint("[KILLPOINTS] You received " .. num .. " points for killing " .. ply:Nick() .. " (" .. ply:GetRoleString() .. ")")
    end
    if not dontTakePoints then
        killer:PS_GivePoints(num)
    end
    end
end

function PointShopDeathHook(ply, killer, dmginfo)
    if not IsValid(killer) or not killer:IsPlayer() or not IsValid(ply) or not ply:IsPlayer() then return end
    if ply == killer then
        ply:ChatPrint("[FUN SERVER] You killed yourself......")
        return
    elseif GetRoundState() == ROUND_ACTIVE then
        NotifyPlayer(ply, killer, dmginfo)
    end
end

hook.Add("DoPlayerDeath", "PointShopDeathHook", PointShopDeathHook)