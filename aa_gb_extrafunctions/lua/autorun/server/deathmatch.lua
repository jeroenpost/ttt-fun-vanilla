deathMatchNextThink = 0
deathMatchRadarScan = 0
activeDeathMatch = false

function startDeathMatch()

    if deathMatchNextThink < CurTime() and GetRoundState() == ROUND_ACTIVE then
        deathMatchNextThink = CurTime() + 15
        alivePlayers = 0
        for k, v in pairs(player.GetAll()) do
            if v:Alive() then alivePlayers = alivePlayers + 1 end
        end

        if alivePlayers == 2 then

            for k, v in pairs(player.GetAll()) do
                if v:Alive() then
                    if v:GetRole() == ROLE_INNOCENT then
                        v:SetRole(ROLE_DETECTIVE)
                        SendFullStateUpdate()
                    end
                    if not v:HasEquipmentItem(EQUIP_RADAR) then

                        v:GiveEquipmentItem(EQUIP_RADAR)
                    end
                    v.radar_charge = 0
                    -- Sometimes doesnt work so check first
                    if v:HasEquipmentItem(EQUIP_RADAR) then
                        v:ConCommand("ttt_radar_scan")
                    end
                    v.radar_charge = 3
                end
                if (not activeDeathMatch) then
                    v:PrintMessage(HUD_PRINTCENTER, "DeathMatch!")

                end
            end
            activeDeathMatch = true
        end

    elseif activeDeathMatch and deathMatchRadarScan < CurTime() then
        deathMatchRadarScan = CurTime() + 1
        for k, v in pairs(player.GetAll()) do
            if v:Alive() then
                if not v:HasEquipmentItem(EQUIP_RADAR) then
                    v:GiveEquipmentItem(EQUIP_RADAR)
                end
                v.radar_charge = 0
                if v:HasEquipmentItem(EQUIP_RADAR) then
                    v:ConCommand("ttt_radar_scan")
                end
                v.radar_charge = 1
            end
        end
    end
end

if SERVER then
    hook.Add("Think", "checkForDeathMatch", startDeathMatch)
    hook.Add("TTTPrepareRound", "resetDeathMatchState", function() activeDeathMatch = false end)
end