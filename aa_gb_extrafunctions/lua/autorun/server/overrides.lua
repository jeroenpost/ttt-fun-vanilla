
hook.Add("InitPostEntity", "pointshopoverridedhook", function()
    local oldkarmafunction = KARMA.IsEnabled
    if oldkarmafunction then
        function KARMA.IsEnabled()
            if SERVER and specialRound.isSpecialRound then return false end
            return oldkarmafunction()
        end
    end

end)

hook.Add("TTTBeginRound","OverRideTimers",function()
    timer.Simple(5,function()
        for k,v in pairs(player.GetAll()) do
            timer.Destroy("lateloadout".. v:EntIndex())
        end
    end)
end)



function protectionSuitDamage2(ent, dmginfo)
    if ent:IsPlayer() then
        local inflictor = dmginfo:GetInflictor( )
        if inflictor:IsPlayer() and (inflictor:IsSpec() or not inflictor:Alive()) and not inflictor:IsGhost() then
            dmginfo:SetDamage(0)
            inflictor:StripWeapons()
        end
    end
end

hook.Add("EntityTakeDamage","checkfordamage", protectionSuitDamage2)