
if SERVER then
	--resource.AddFile("materials/vgui/ttt/gestures.png")
end
gb_gestures = {}
gb_gestures["yes"] = ACT_GMOD_GESTURE_AGREE
gb_gestures["no"] = ACT_GMOD_GESTURE_DISAGREE
gb_gestures["wave"] = ACT_GMOD_GESTURE_WAVE
gb_gestures["follow"] = ACT_GMOD_GESTURE_BECON
gb_gestures["bow"] = ACT_GMOD_GESTURE_BOW
gb_gestures["forward"] = ACT_SIGNAL_FORWARD
gb_gestures["halt"] = ACT_SIGNAL_HALT
gb_gestures["group"] = ACT_SIGNAL_GROUP
gb_gestures["muscle"] = ACT_GMOD_TAUNT_MUSCLE
gb_gestures["cheer"] = ACT_GMOD_TAUNT_CHEER 
gb_gestures["laugh"] = ACT_GMOD_TAUNT_LAUGH 
gb_gestures["persist"] = ACT_GMOD_TAUNT_PERSISTENCE 

gb_gestures["robot"] = ACT_GMOD_TAUNT_ROBOT 
gb_gestures["dance"] = ACT_GMOD_TAUNT_DANCE 
gb_gestures["zombie"] = ACT_GMOD_TAUNT_ZOMBIE 


function GB_Gestures(ply, text, isTeamChat) 
	

    if ply:Alive() and text == "!thirdperson" then
        ply:ConCommand("hat_thirdperson");
    end
       

	if ply:Alive() && text:sub(1,1) == "/" then
		
                text = text:lower()
		
                if !ply.canGesture then		
			return text.." (I should buy gestures from the Pointshop..)"
		end
		
		
		local isGest
		
		for lable, comm in pairs(gb_gestures) do
			isGest = string.Replace( lable , " ", "")
			isGest = string.lower(isGest)
			
			-- if This Act is the User Command
			if text:sub(2) == isGest then


				ply:AnimPerformGesture_gb( comm )
			    return ""
			end	
		end
	end
  
end



hook.Add("PlayerSay", "GreenBlack TTT Gestures", GB_Gestures)



function GB_showGestures( player, command, arguments)
	player:ChatPrint("==================\nGesture list:")
	player:ChatPrint("==================")
	for lable, comm in pairs(gb_gestures) do
		player:ChatPrint("/"..lable)		
	end
	player:ChatPrint("==================")

end

concommand.Add("show_gestures", GB_showGestures)



local plymeta = FindMetaTable( "Player" )
if not plymeta then return end
if CLIENT then

   local function MakeSimpleRunner(act)
        return function (ply, w)
        -- just let this gesture play itself and get out of its way
            if w == 0 then
                ply:AnimApplyGesture(act, 1)
                return 1
            else
                return 0
            end
        end
    end

    -- act -> gesture runner fn
    local act_runner = {
        -- ear grab needs weight control
        -- sadly it's currently the only one
        [ACT_GMOD_IN_CHAT] =
        function (ply, w)
            local dest = ply:IsSpeaking() and 1 or 0
            w = math.Approach(w, dest, FrameTime() * 10)
            if w > 0 then
                ply:AnimApplyGesture(ACT_GMOD_IN_CHAT, w)
            end
            return w
        end
    };

    -- Insert all the "simple" gestures that do not need weight control
    for _, a in pairs{ACT_GMOD_GESTURE_AGREE, ACT_GMOD_GESTURE_DISAGREE,
        ACT_GMOD_GESTURE_WAVE, ACT_GMOD_GESTURE_BECON,
        ACT_GMOD_GESTURE_BOW, ACT_GMOD_GESTURE_SALUTE,
        ACT_GMOD_CHEER, ACT_SIGNAL_FORWARD, ACT_SIGNAL_HALT,
        ACT_SIGNAL_GROUP, ACT_ITEM_PLACE, ACT_ITEM_DROP,
        ACT_ITEM_GIVE, ACT_GMOD_TAUNT_ROBOT,ACT_GMOD_TAUNT_DANCE,
        ACT_GMOD_TAUNT_CHEER, ACT_GMOD_TAUNT_LAUGH,
        ACT_GMOD_TAUNT_PERSISTENCE,ACT_GMOD_IN_CHAT,
        ACT_WALK_SCARED, ACT_VICTORY_DANCE,
        ACT_GMOD_SIT_ROLLERCOASTER, ACT_GMOD_GESTURE_POINT} do
        act_runner[a] = MakeSimpleRunner(a)
    end

      -- Perform the gesture using the GestureRunner system. If custom_runner is
    -- non-nil, it will be used instead of the default runner for the act.
    function plymeta:AnimPerformGesture_gb(act)
        local runner =  act_runner[act]
        if not runner then return false end

        self.GestureWeight = 0
        self.GestureRunner = runner

        return true
    end

    -- Perform a gesture update
    function plymeta:AnimUpdateGesture_gb()
        if self.GestureRunner then
            self.GestureWeight = self:GestureRunner(self.GestureWeight)

            if self.GestureWeight <= 0 then
                self.GestureRunner = nil
            end
        end
    end

    function gamemode:GrabEarAnimation(ply)
        ply.ChatGestureWeight = ply.ChatGestureWeight or 0

        if ( ply:IsSpeaking() ) then
            ply.ChatGestureWeight = math.Approach( ply.ChatGestureWeight, 1, FrameTime() * 10.0 );
        else
            ply.ChatGestureWeight = math.Approach( ply.ChatGestureWeight, 0, FrameTime()  * 10.0 );
        end

        if ( ply.ChatGestureWeight > 0 ) then

            ply:AnimRestartGesture( GESTURE_SLOT_CUSTOM, ACT_GMOD_IN_CHAT )
            ply:AnimSetGestureWeight( GESTURE_SLOT_CUSTOM, ply.ChatGestureWeight )

        end
    end

    net.Receive("TTT_PerformGesture2", function()
        local ply = net.ReadEntity()
        local act = net.ReadUInt(16)
        if IsValid(ply) and act then
            ply:AnimPerformGesture_gb(act)
        end
    end)

else -- SERVER
    util.AddNetworkString( "TTT_PerformGesture2" )
    -- On the server, we just send the client a message that the player is
    -- performing a gesture. This allows the client to decide whether it should
    -- play, depending on eg. a cvar.
    function plymeta:AnimPerformGesture_gb(act)

        if not act then return end

        net.Start("TTT_PerformGesture2")
        net.WriteEntity(self)
        net.WriteUInt(act, 16)
        net.Broadcast()
    end
end

