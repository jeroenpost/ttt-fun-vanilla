SWEP.Base = "aa_base"
SWEP.ViewModelFOV	= 54
SWEP.ViewModelFlip	= false
SWEP.UseHands       = true
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable		= false

SWEP.HoldType = "shotgun"
SWEP.PrintName = "Tester"
SWEP.Camo = 0
SWEP.NumCamos = 52
SWEP.WeaponColor = Color(255,255,255,255)
SWEP.Kind = WEAPON_EQUIP3
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_famas.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_famas.mdl"

SWEP.Primary.Damage		    = 10
SWEP.Primary.Delay		    = 0.1
SWEP.Primary.ClipSize		= 100
SWEP.Primary.DefaultClip	= 100
SWEP.Primary.Cone	        = 0.001
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"
SWEP.Primary.Sound          = Sound("Weapon_AR2.Single")


SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0,0,0)

SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.AllowDrop = true
SWEP.IsSilent = true
SWEP.NoSights = false

function SWEP:InitializeFunction()
    self:SetHoldType( self.HoldType or "pistol" )
    if self.Camo == 0 then
        local test = math.random(1,self.NumCamos*5 )
        if server_id > 99 && server_id < 150 then -- Vanilla servers
             test = math.random(1,self.NumCamos*12 )
        end


        if test <= self.NumCamos then
            self.Camo = test
            self:SetNWInt("camo",self.Camo)
            self:SetColorAndMaterial( self.WeaponColor, "camos/camo"..self:GetNetworkedInt("camo",0) );
        else
            self:SetNWInt("camo",0)
            self.Camo = false
        end
    else
        self:SetNWInt("camo",self.Camo)
        self:SetColorAndMaterial( self.WeaponColor, "camos/camo"..self:GetNetworkedInt("camo",0) );
    end

end

function SWEP:Initialize()
    self:InitializeFunction()

end

function SWEP:SetPrintName( name )
    self.PrintName = name
    self:SetNWString("PrintName", name)
end

function SWEP:SetCamo( camo )
    self.Camo = camo
    self:SetNWInt("camo",camo)
    self:SetColorAndMaterial( self.WeaponColor, "camos/camo"..camo );
end


function SWEP:DeployFunction()
    local printname =  self:GetNetworkedString("PrintName")
    if printname  then
        self.PrintName = printname
    end
    if  self:GetNetworkedInt("camo") > 0 then
         self:SetColorAndMaterial( self.WeaponColor, "camos/camo"..self:GetNetworkedInt("camo",0) );
    end
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    return true
end

-- COLOR
function SWEP:Deploy()
    return self:DeployFunction()
end

function SWEP:Reload()
    if self.NextReload and self.NextReload > CurTime() then return end
    self.NextReload = CurTime() + 1
    if self.Owner:KeyDown(IN_DUCK) then
        self:SetNWInt("camo",0)
        return
    end
    if ( self:Clip1() == self.Primary.ClipSize or self.Owner:GetAmmoCount( self.Primary.Ammo ) <= 0 ) then return end
    self:DefaultReload(self.ReloadAnim)
    self:SetIronsights( false )
end

function SWEP:PreDrop()
    self:ColorReset()
end

function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    if not isfunction(self.ColorReset) then return end
    self:ColorReset()
end

function SWEP:OnDrop()
    self:ColorReset()
end

function SWEP:PreDrawViewModel()
    if SERVER or  self:GetNetworkedInt("camo") < 1  or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor( self.WeaponColor )
    self.Owner:GetViewModel():SetMaterial(  "camos/camo"..self:GetNetworkedInt("camo",0)  )
end
