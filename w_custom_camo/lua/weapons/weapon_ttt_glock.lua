SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.Icon = "VGUI/ttt/icon_glock"
SWEP.PrintName = "Glock 18"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_glock18.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_glock18.mdl"
SWEP.Kind = WEAPON_PISTOL
SWEP.Slot = 1
SWEP.AutoSpawnable = true
SWEP.Primary.Recoil	= 0.9
SWEP.Primary.Damage = 12
SWEP.Primary.Delay = 0.10
SWEP.Primary.Cone = 0.028
SWEP.Primary.ClipSize = 20
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 20
SWEP.Primary.ClipMax = 60
SWEP.Primary.Ammo = "Pistol"
SWEP.AutoSpawnable = true
SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Primary.Sound = Sound( "Weapon_Glock.Single" )
SWEP.IronSightsPos = Vector( -5.79, -3.9982, 2.8289 )

SWEP.HeadshotMultiplier = 1.75
