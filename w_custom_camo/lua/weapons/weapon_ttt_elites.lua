SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "duel"
SWEP.PrintName = "Elites"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_elite.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_elite.mdl"
SWEP.Kind = WEAPON_PISTOL
SWEP.Slot = 1
SWEP.AutoSpawnable = true
SWEP.Primary.Recoil     = 1.5
SWEP.Primary.Damage = 30
SWEP.Primary.Delay = 0.40
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 20
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 20

SWEP.Secondary.Delay = 0.40
SWEP.Secondary.ClipSize     = 20
SWEP.Secondary.DefaultClip  = 20
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = "Pistol"
SWEP.Secondary.ClipMax      = 60

SWEP.Primary.Ammo = "smg1"
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.HeadShotMultiplier = 1



SWEP.Primary.Sound = Sound( "Weapon_Elite.Single" )
SWEP.Secondary.Sound = Sound( "Weapon_Elite.Single" )




function SWEP:PrimaryAttack( worldsnd )

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:CanSecondaryAttack()
    if not IsValid(self.Owner) then return end

    if self.Weapon:Clip1() <= 0 then
        self:DryFire(self.SetNextSecondaryFire)
        return false
    end
    return true
end

function SWEP:SecondaryAttack(worldsnd)


    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    --self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanSecondaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    -- owner:ViewPunch( Angle( math.Rand(-0.6,-0.6) * self.Primary.Recoil, math.Rand(-0.6,0.6) *self.Primary.Recoil, 0 ) )

end
