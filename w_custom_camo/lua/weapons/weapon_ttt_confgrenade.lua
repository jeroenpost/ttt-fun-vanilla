SWEP.Base = "gb_camo_base_grenade"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "grenade"
SWEP.PrintName = "Discomb Grenade"
SWEP.ViewModel			= "models/weapons/cstrike/c_eq_fraggrenade.mdl"
SWEP.WorldModel			= "models/weapons/w_eq_fraggrenade.mdl"
SWEP.Kind = WEAPON_NADE
SWEP.Primary.Delay		    = 1
SWEP.Primary.Sound          = Sound("npc/waste_scanner/grenade_fire.wav")
SWEP.AutoSpawnable = true

function SWEP:GetGrenadeName()
    return "ttt_confgrenade_proj"
end

function SWEP:PrimaryAttack()
    if SERVER then
        DamageLog("Discomb: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] threw a grenade")
    end
    self.BaseClass.PrimaryAttack(self)
end
