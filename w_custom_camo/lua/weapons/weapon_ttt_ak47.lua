SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "AK 47"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_ak47.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_ak47.mdl"
SWEP.AutoSpawnable = true
SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
SWEP.Kind = WEAPON_HEAVY
SWEP.Slot = 2
SWEP.Primary.Delay = 0.12
SWEP.Primary.Recoil = 2.0
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 18
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 30
SWEP.Primary.ClipMax = 90
SWEP.Primary.DefaultClip = 30
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.Primary.Sound = Sound("weapons/ak47/ak47-1.wav")

SWEP.HeadshotMultiplier = 1.5

SWEP.IronSightsPos = Vector( -6.6, -8.50, 3.40 )
SWEP.IronSightsAng = Vector( 0, 0, 0 )