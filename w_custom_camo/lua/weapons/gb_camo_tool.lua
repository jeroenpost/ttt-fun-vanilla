SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.PrintName = "Tooly"
SWEP.ViewModel		= "models/weapons/c_toolgun.mdl"
SWEP.WorldModel		= "models/weapons/w_toolgun.mdl"

SWEP.Primary.Damage		    = 15
SWEP.Primary.Delay		    = 0.1
SWEP.Primary.ClipSize		= 20
SWEP.Primary.DefaultClip	= 20
SWEP.Primary.Cone	        = 0.001
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "smg"
SWEP.Primary.Sound          = Sound("weapons/smg1/smg1_fireburst1.wav")