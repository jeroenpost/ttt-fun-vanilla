SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "M249"
SWEP.ViewModel		= "models/weapons/cstrike/c_mach_m249para.mdl"
SWEP.WorldModel		= "models/weapons/w_mach_m249para.mdl"
SWEP.AutoSpawnable = true

SWEP.Primary.Damage = 7
SWEP.Primary.Delay = 0.06
SWEP.Primary.Cone = 0.09
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo        = "smg1"
SWEP.AutoSpawnable      = true
SWEP.Primary.Sound			= Sound("Weapon_m249.Single")
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.Primary.ClipSize = 75
SWEP.Primary.ClipMax = 250
SWEP.Primary.DefaultClip	= 75
SWEP.Primary.Recoil			= 1

SWEP.HoldType = "crossbow"
SWEP.Slot = 2
SWEP.Kind = WEAPON_HEAVY

SWEP.HeadshotMultiplier = 2.2

SWEP.IronSightsPos = Vector(-5.96, -5.119, 2.349)
SWEP.IronSightsAng = Vector(0, 0, 0)