SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Silenced TMP"
SWEP.ViewModel		= "models/weapons/cstrike/c_smg_tmp.mdl"
SWEP.WorldModel		= "models/weapons/w_smg_tmp.mdl"
SWEP.AutoSpawnable = true

SWEP.HoldType = "smg"
SWEP.Slot = 2

if CLIENT then
    SWEP.Author = "GreenBlack"
    SWEP.Icon = "vgui/ttt_fun_killicons/tmp.png"
end

SWEP.Primary.Damage      = 16
SWEP.Primary.Delay       = 0.095
SWEP.Primary.Cone        = 0.06
SWEP.Primary.ClipSize    = 30
SWEP.Primary.ClipMax     = 60
SWEP.Primary.DefaultClip = 30
SWEP.Primary.Automatic   = true
SWEP.Primary.Ammo        = "smg1"
SWEP.Primary.Recoil      = 1.7
SWEP.AutoSpawnable      = true
SWEP.Kind = WEAPON_HEAVY

SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.IsSilent = true

SWEP.Primary.Sound = Sound( "Weapon_tmp.Single" )
SWEP.Primary.SoundLevel = 50

SWEP.IronSightsPos = Vector( -6.9, -8, 2.6091 )
SWEP.IronSightsAng = Vector( 0, 0, 0 )