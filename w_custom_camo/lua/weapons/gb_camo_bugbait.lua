SWEP.Base = "gb_camo_base_grenade"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "slam"
SWEP.PrintName = "Bugbait"
SWEP.ViewModel		= "models/weapons/c_bugbait.mdl"
SWEP.WorldModel		= "models/weapons/w_bugbait.mdl"
SWEP.AutoSpawnable = true
SWEP.Kind = WEAPON_NADE
SWEP.Primary.Delay		    = 1
SWEP.Primary.Sound          = Sound("weapons/bugbait/bugbait_squeeze1.wav")


SWEP.GrenadeEnt = "camo_bugbait_thrown"

SWEP.Sounds =
{
    Squeeze1 = Sound("weapons/bugbait/bugbait_squeeze1.wav"),
    Squeeze2 = Sound("weapons/bugbait/bugbait_squeeze2.wav"),
    Squeeze3 = Sound("weapons/bugbait/bugbait_squeeze3.wav")
}

function SWEP:SecondaryAttack()
    self.Weapon:SetNextSecondaryFire(CurTime() + 1);
    if SERVER then
      self.Owner:EmitSound(self.Sounds["Squeeze"..math.random(1,3)]);
    end
    self.Weapon:SendWeaponAnim(ACT_VM_SECONDARYATTACK);
end

function SWEP:PrimaryAttack()
    if (not self.Next or self.Next < CurTime()) and (not self.Primed or self.Primed == 0)  then
        self.Next = CurTime() + self.Primary.Delay
        self.Weapon:SendWeaponAnim(ACT_VM_PULLPIN)
        self.Primed = 1

            if SERVER then
                DamageLog("Bugbait: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] threw a grenade")
            end

    end
end

function SWEP:Think()
    if not self.Next or self.Next < CurTime() then
        if self.Primed == 1 and not self.Owner:KeyDown(IN_ATTACK) then
            self.Weapon:SendWeaponAnim(ACT_VM_THROW)
            self.Primed = 2
            self.Next = CurTime() + .3
        elseif self.Primed == 2 then
            self.Primed = 0
            self.Next = CurTime() + self.Primary.Delay

            if SERVER then
                local ent = ents.Create(self.GrenadeEnt)
                ent:SetOwner(self.Owner)
                ent.Owner = self.Owner
                ent:SetPos(self.Owner:GetShootPos())
                ent:SetAngles(Angle(1,0,0))
                ent:SetMaterial(  "camos/camo"..self:GetNetworkedInt("camo",1) )
                if (self.Weapon:GetNWBool("upgraded") and SERVER) then
                    ent:Upgrade()
                    ent:SetNWBool("upgraded", true)
                end
                ent:Spawn()

                local phys = ent:GetPhysicsObject()
                phys:SetVelocity(self.Owner:GetAimVector() * 1000)
                phys:AddAngleVelocity(Vector(math.random(-1000,1000),math.random(-1000,1000),math.random(-1000,1000)))

                self.Owner:RemoveAmmo(1,self.Primary.Ammo)
                self:Remove()
                if self.Owner:GetAmmoCount(self.Primary.Ammo) > 0 then
                    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
                end
            end
        end
    end
end

function SWEP:GetGrenadeName()
    return "ttt_smokegrenade_proj"
end
