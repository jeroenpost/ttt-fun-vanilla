
include "weapons/weapon_tttbasegrenade.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFlip	= false
SWEP.Kind = WEAPON_NADE
SWEP.Slot = 3

function SWEP:GetGrenadeName()
    return "ttt_firegrenade_proj"
end

function SWEP:Initialize()
    if self.SetHoldType then
        self:SetHoldType( self.HoldType or "pistol" )
    end

    self:SetDeploySpeed(self.DeploySpeed)

    self:SetDetTime(0)
    self:SetThrowTime(0)
    self:SetPin(false)

    self.was_thrown = false
    self:InitializeFunction()
end
SWEP.OldCreateGrenade = SWEP.CreateGrenade
function SWEP:CreateGrenade(src, ang, vel, angimp, ply)
    local nade = self:OldCreateGrenade(src, ang, vel, angimp, ply)
    nade:SetMaterial( "camos/camo"..self:GetNetworkedInt("camo",1))
end


function SWEP:Deploy()
    self:DeployFunction()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    return true
end

function SWEP:SecondaryAttack()
    return false
end