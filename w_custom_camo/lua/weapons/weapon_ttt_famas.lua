SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Famas"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_famas.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_famas.mdl"


SWEP.HoldType = "ar2"
SWEP.Slot = 2

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/famas.png"
end

SWEP.Kind = WEAPON_HEAVY

SWEP.Primary.Damage = 12
SWEP.Primary.Delay = 0.09
SWEP.Primary.Cone = 0.03
SWEP.Primary.ClipSize = 25
SWEP.Primary.ClipMax = 75
SWEP.Primary.DefaultClip = 25
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Recoil = 1.2
SWEP.Primary.Sound = Sound( "weapons/famas/famas-1.wav" )
SWEP.AutoSpawnable = true
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.IronSightsPos = Vector( -6.3, -3.28, 0.01 )
SWEP.IronSightsAng = Vector( 0,0,0)

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
    local att = dmginfo:GetAttacker()
    if not IsValid(att) then return 2 end

    local dist = victim:GetPos():Distance(att:GetPos())
    local d = math.max(0, dist - 150)

    return 1.5 + math.max(0, (1.5 - 0.002 * (d ^ 1.25)))
end