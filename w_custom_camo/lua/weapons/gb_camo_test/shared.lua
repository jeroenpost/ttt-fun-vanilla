
AddCSLuaFile( "shared.lua" )

SWEP.ViewModelFOV	= 62
SWEP.ViewModelFlip	= false
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_famas.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_famas.mdl"
SWEP.UseHands = true
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable		= false

SWEP.Primary.ClipSize		= 300000
SWEP.Primary.DefaultClip	= 500000
SWEP.Primary.Cone	= 0.001
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"
SWEP.Primary.Sound          = Sound("Weapon_AR2.Single")

SWEP.Secondary.ClipSize		= 3
SWEP.Secondary.DefaultClip	= 32
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)


SWEP.HoldType = "shotgun"
SWEP.PrintName = "Tester"

SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_EQUIP3
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.InLoadoutFor = nil
SWEP.AllowDrop = true
SWEP.IsSilent = true
SWEP.NoSights = false

function SWEP:ShootBullet( damage, num_bullets, aimcone )

local bullet = {}
    bullet.Num 		= num_bullets
    bullet.Src 		= self.Owner:GetShootPos()			
    bullet.Dir 		= self.Owner:GetAimVector()
    bullet.Spread 	= Vector( 0.001, 0.001, 0 )
    bullet.Tracer	= 1
    bullet.Force	= 10
    bullet.Damage	= damage
    bullet.AmmoType = "Pistol"
    bullet.HullSize = 2
    bullet.TracerName = "LaserTracer_thick"

    self.Owner:FireBullets( bullet )
    self:detachLimb()
    self:ShootEffects()

end

SWEP.LaserColor = Color(120,120,0,255)

-- COLOR
function SWEP:Deploy()
    self.WorldModel		= string.Replace("models/weapons/"..self.ViewModels[self.NumVieModel],"c_","w_")
    self:SetModel( self.WorldModel )
    self.Owner:PrintMessage(HUD_PRINTCENTER,"Reload to switch model, RMB to switch camo")
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self:SetColorAndMaterial(Color(255,255,255,255),   "camos/camo"..self.Camo );
    return true
end


function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end
function SWEP:SecondaryAttack()
   self:SwitchCamo()
end

SWEP.Camo = 40
function SWEP:SwitchCamo()
    if  self.NextCamo and self.NextCamo > CurTime() then return end
    self.NextCamo = CurTime() + 0.05
        self.Camo = self.Camo + 1
    if self.Camo > 51 then
        self.Camo = 1
    end
    self.Owner:PrintMessage(HUD_PRINTCENTER,"Camo "..self.Camo)

    self:SetColorAndMaterial(Color(255,255,255,255),   "camos/camo"..self.Camo  );

    self.PrintName = "Camo test "..self.Camo

        if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:PrintMessage(HUD_PRINTTALK ,"Camo "..self.Camo)
        self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
        self.Owner:GetViewModel():SetMaterial(  "camos/camo"..self.Camo  )

end





SWEP.ViewModels = {
"c_bugbait.mdl",
"c_crossbow.mdl",
"c_crowbar.mdl",
"c_grenade.mdl",
"c_irifle.mdl",
"c_medkit.mdl",
"c_physcannon.mdl",
"c_pistol.mdl",
"c_rpg.mdl",
"c_shotgun.mdl",
"c_slam.mdl",
"c_smg1.mdl",
"c_stunstick.mdl",
"c_superphyscannon.mdl",
"c_toolgun.mdl",
"c_357.mdl",

"c_c4.mdl",
"c_eq_flashbang.mdl"
,"c_eq_fraggrenade.mdl"
,"c_eq_smokegrenade.mdl"
,"c_knife_t.mdl"
,"c_mach_m249para.mdl"
,"c_pist_deagle.mdl"
,"c_pist_elite.mdl"
,"c_pist_fiveseven.mdl"
,"c_pist_glock18.mdl"
,"c_pist_p228.mdl"
,"c_pist_usp.mdl"
,"c_rif_ak47.mdl"
,"c_rif_aug.mdl"
,"c_rif_famas.mdl"
,"c_rif_galil.mdl"
,"c_rif_m4a1.mdl"
,"c_rif_sg552.mdl"
,"c_shot_m3super90.mdl"
,"c_shot_xm1014.mdl"
,"c_smg_mac10.mdl"
,"c_smg_mp5.mdl"
,"c_smg_p90.mdl"
,"c_smg_tmp.mdl"
,"c_smg_ump45.mdl"
,"c_snip_awp.mdl"
,"c_snip_g3sg1.mdl"
,"c_snip_scout.mdl"
,"c_snip_sg550.mdl"
}
SWEP.NumVieModel = 1
function SWEP:Reload()
    self:SwitchViewModel()
end

function SWEP:SwitchViewModel()
    if  self.NextCamo and self.NextCamo > CurTime() then return end
    self.NextCamo = CurTime() + 0.2
    self.NumVieModel =  self.NumVieModel + 1
    if self.NumVieModel > 45 then
     self.NumVieModel = 1
    end
    if self.NumVieModel > 16 then
        if not self.ViewModels[self.NumVieModel] then print("model "..self.NumVieModel.." not found") end
    self.ViewModel		= "models/weapons/cstrike/"..self.ViewModels[self.NumVieModel]
    self.WorldModel		= string.Replace("models/weapons/"..self.ViewModels[self.NumVieModel],"c_","w_")
    else
        self.ViewModel		= "models/weapons/"..self.ViewModels[self.NumVieModel]
        self.WorldModel		= string.Replace("models/weapons/"..self.ViewModels[self.NumVieModel],"c_","w_")
    end
    self:SetModel( self.WorldModel )
    self.Owner:GetViewModel():SetModel(  self.ViewModel  )
    self.Owner:PrintMessage(HUD_PRINTCENTER,"Model: "..self.ViewModels[self.NumVieModel])
    if not SERVER then return end
    self.Owner:PrintMessage(HUD_PRINTTALK ,"Model: "..self.ViewModels[self.NumVieModel])
end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial(  "camos/camo"..self.Camo  )

end

function SWEP:detachLimb(  )
    if not self.bones then
        self.bones = { "ValveBiped.Bip01_Head1", "ValveBiped.Bip01_R_Hand", "ValveBiped.Bip01_R_Forearm", "ValveBiped.Bip01_R_Foot", "ValveBiped.Bip01_R_Thigh", "ValveBiped.Bip01_R_Calf",  "ValveBiped.Bip01_R_Elbow", "ValveBiped.Bip01_R_Shoulder"
            , "ValveBiped.Bip01_L_Hand", "ValveBiped.Bip01_L_Forearm", "ValveBiped.Bip01_L_Foot", "ValveBiped.Bip01_L_Thigh", "ValveBiped.Bip01_L_Calf",  "ValveBiped.Bip01_L_Elbow", "ValveBiped.Bip01_L_Shoulder" }
    end

    local trace = self.Owner:GetEyeTrace()
    if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 9000 then
        if( trace.Entity:IsPlayer() or trace.Entity:IsNPC() or trace.Entity:GetClass()=="prop_ragdoll" ) then

            timer.Simple(0.01,function()

                local bone = table.Random(self.bones)
                bone = trace.Entity:LookupBone( bone )
                if bone then
                    local Ply = trace.Entity
                    local Normal = bone
                    Ply:ManipulateBoneScale(Normal, vector_origin)
                    Ply:EmitSound( "weapons/shortsword/morrowind_shortsword_hit.mp3" )

                    local ED = EffectData()
                    ED:SetEntity( Ply ) --Player Entity
                    ED:SetNormal( self.Owner:GetForward() )
                    ED:SetScale( Normal )
                    ED:SetOrigin( Ply:GetPos() )
                    util.Effect( 'limbremove', ED ) --Effect Gib
                end

            end)
        end
    end


end