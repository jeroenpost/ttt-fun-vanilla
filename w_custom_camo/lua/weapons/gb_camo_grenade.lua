SWEP.Base = "gb_camo_base_grenade"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "grenade"
SWEP.PrintName = "Grenade"
SWEP.ViewModel		= "models/weapons/c_grenade.mdl"
SWEP.WorldModel		= "models/weapons/w_grenade.mdl"

SWEP.Primary.Delay		    = 1
SWEP.Primary.Sound          = Sound("npc/waste_scanner/grenade_fire.wav")


function SWEP:SecondaryAttack()
   return false
end

function SWEP:PrimaryAttack()
    if SERVER then
        DamageLog("Grenade: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] threw a grenade")
    end
    self.BaseClass.PrimaryAttack(self)
end
