include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Spas12"
SWEP.ViewModel		= "models/weapons/c_shotgun.mdl"
SWEP.WorldModel		= "models/weapons/w_shotgun.mdl"


SWEP.HoldType = "shotgun"
SWEP.Slot = 2
SWEP.Kind = WEAPON_HEAVY

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 7

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 14
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 1
SWEP.Primary.ClipSize = 8
SWEP.Primary.ClipMax = 24
SWEP.Primary.DefaultClip = 8
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 8
SWEP.AutoSpawnable      = true
SWEP.AmmoEnt = "item_box_buckshot_ttt"

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return self:DeployFunction()
end

