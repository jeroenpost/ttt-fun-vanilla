
AddCSLuaFile( "shared.lua" )

include('shared.lua')

function ENT:Initialize()

	self.Entity:SetModel("models/weapons/w_bugbait.mdl")
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:DrawShadow( false )
	
	// Don't collide with the player
	// too bad this doesn't actually work.
	self.Entity:SetCollisionGroup( COLLISION_GROUP_WEAPON )
	
	local phys = self.Entity:GetPhysicsObject()
	
	if (phys:IsValid()) then
		phys:Sleep()
	end
	
	self.timer = CurTime() + 4
	self.solidify = CurTime() + 1
	self.Bastardgas = nil
	self.Spammed = false
end
function Poison(ply)
	if (ply:Health() >= 0) then
		ply:SetHealth(ply:Health() - 5)
	elseif (ply:Alive()) and (ply:Health() <= 0)then
		ply:Kill()
		else
	end
end

function ENT:Think()
	if (IsValid(self.Owner)==false) then
		self.Entity:Remove()
	end
	if (self.solidify<CurTime()) then
		self.SetOwner(self.Entity)
	end
	if self.timer < CurTime() then
		if !IsValid(self.Bastardgas) && !self.Spammed then
			self.Spammed = true
			self.Bastardgas = ents.Create("env_smoketrail")
			self.Bastardgas:SetPos(self.Entity:GetPos())
			self.Bastardgas:SetKeyValue("spawnradius","256")
			self.Bastardgas:SetKeyValue("minspeed","0.5")
			self.Bastardgas:SetKeyValue("maxspeed","2")
			self.Bastardgas:SetKeyValue("startsize","16536")
			self.Bastardgas:SetKeyValue("endsize","256")
			self.Bastardgas:SetKeyValue("endcolor","149 129 82")
			self.Bastardgas:SetKeyValue("startcolor","125 85 32")
			self.Bastardgas:SetKeyValue("opacity","2")
			self.Bastardgas:SetKeyValue("spawnrate","15")
			self.Bastardgas:SetKeyValue("lifetime","5")
			self.Bastardgas:SetParent(self.Entity)
			self.Bastardgas:Spawn()
			self.Bastardgas:Activate()
			self.Bastardgas:Fire("turnon","", 0.1)
			local exp = ents.Create("env_explosion")
			exp:SetKeyValue("spawnflags",461)
			exp:SetPos(self.Entity:GetPos())
			exp:Spawn()
			exp:Fire("explode","",0)
			self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))
		end

		local pos = self.Entity:GetPos()
		local maxrange = 100
		local maxstun = 3
		for k,v in pairs(player.GetAll()) do
			local plpos = v:GetPos()
			local dist = -pos:Distance(plpos)+maxrange
			if (pos:Distance(plpos)<=maxrange) then
				local trace = {}
					trace.start = self.Entity:GetPos()
					trace.endpos = v:GetPos()+Vector(0,0,24)
					trace.filter = { v, self.Entity }
					trace.mask = COLLISION_GROUP_PLAYER
				tr = util.TraceLine(trace)
				if (tr.Fraction==1) then
					local stunamount = math.ceil(dist/(maxrange/maxstun))
					 v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
					Poison(v, stunamount, self.Owner, self.Owner:GetWeapon("weapon_gasgrenade"))
				end
			end
		end
		if (self.timer+5<CurTime()) then
			if IsValid(self.Bastardgas) then
				self.Bastardgas:Remove()
                self:ExplodeHard()
			end
		end
		if (self.timer+8<CurTime()) then

		end
		self.Entity:NextThink(CurTime()+0.2)
		return true
	end
end

function ENT:ExplodeHard()

    if not IsValid( self.Owner ) then return end

    local ent = ents.Create( "env_explosion" )

    ent:SetPos( self:GetPos()  )
        ent:SetPhysicsAttacker(  self.Owner )

    ent:Spawn()
    ent:SetKeyValue( "iMagnitude", "50" )
    ent:Fire( "Explode", 0, 0 )

    util.BlastDamage( self,  self.Owner, self:GetPos(), 100, 50 )
    ent:EmitSound( "weapons/big_explosion.mp3" )
    self:Remove()
end
