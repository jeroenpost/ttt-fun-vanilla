CATEGORY.Name = 'Donator Playermodels'
CATEGORY.Desc = 'Playermodels for Donators. Donators only pay $2.5/mo!'
CATEGORY.Icon = 'key'
function CATEGORY:CanPlayerBuy(ply) return gb.is_donator(ply) end
CATEGORY.Order = 3
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "playermodels"
CATEGORY.buyWhenDeath = true