CATEGORY.Name = 'Superdonator Boosts/Powers'
CATEGORY.Desc = 'Superdonators can buy these boosts and powers. Say !donate in chat to donate too!'
CATEGORY.Icon = 'key'

CATEGORY.Order = 6
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "roles"
CATEGORY.CanPlayerSee = function() return true end
function CATEGORY:CanPlayerBuy(ply) return gb.is_superdonator(ply) end