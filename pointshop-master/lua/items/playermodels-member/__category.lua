CATEGORY.Name = 'Member Playermodels'
CATEGORY.Desc = 'Play 10 hours on this server and you get access to these models'
CATEGORY.Icon = 'key'

CATEGORY.Order = 2
CATEGORY.CanPlayerSee = function() return true end
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "playermodels"
CATEGORY.buyWhenDeath = true

function CATEGORY:CanPlayerBuy(ply) return gb.IsMember(ply) end