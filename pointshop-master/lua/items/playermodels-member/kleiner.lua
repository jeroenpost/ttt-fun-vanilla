ITEM.Name = 'Kleiner'
ITEM.Price = 7000
ITEM.Model = 'models/player/kleiner.mdl'
ITEM.AdminOnly = false
ITEM.SingleUse = false

function ITEM:OnEquip(ply, modifications)
	if not ply._OldModel then
		ply._OldModel = ply:GetModel()
	end
	hook.Remove("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel")
	hook.Add("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel", function()
	   if ply then
		ply:SetHealth(55)
		ply:PrintMessage( HUD_PRINTTALK, "You have playermodel Kleiner. This model is harder to shoot, so health is set to 55")
       end
	end)

	
	timer.Simple(1, function() ply:SetModel(self.Model) end)
end

function ITEM:OnHolster(ply)
	if ply._OldModel then
		ply:SetModel(ply._OldModel)
	end
	hook.Remove("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel")

end

function ITEM:OnSell(ply)
hook.Remove("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel")

end