CATEGORY.Name = 'Donator Weapons'
CATEGORY.Desc = 'Weapons for Donators. For just $2.50/mo you have access to all donator stuff!'
CATEGORY.Icon = 'user_suit'
CATEGORY.Order = 6
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "weapons"
CATEGORY.CanPlayerSee = function() return true end
function CATEGORY:CanPlayerBuy(ply) return gb.is_donator(ply) end