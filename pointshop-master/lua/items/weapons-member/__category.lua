CATEGORY.Name = 'Member Weapons'
CATEGORY.Desc = 'Weapons for Members. You become a member after 10 hours playtime'
CATEGORY.Icon = 'user_suit'
function CATEGORY:CanPlayerBuy(ply) return gb.IsMember(ply) end
CATEGORY.Order = 5
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "weapons"
CATEGORY.CanPlayerSee = function() return true end