CATEGORY.Name = 'Member Boosts/Powers'
CATEGORY.Desc = 'Members can buy these boosts and powers. You become member with 10 hours playtime'
CATEGORY.Icon = 'key'
CATEGORY.Order = 2
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "roles"
CATEGORY.CanPlayerSee = function() return true end
function CATEGORY:CanPlayerBuy(ply) return gb.IsMember(ply) end