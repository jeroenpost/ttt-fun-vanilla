ITEM.Name = '+10% Stamina'
ITEM.Price = 5000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/fun_bull.png'
ITEM.Description = [[Speeds up the regen of your stamina]]

function ITEM:OnEquip(ply)
    if ply:GetNWInt("staminaboost",0) < 10  then
        ply:SetNWInt("staminaboost",0.10)
    end
end 
