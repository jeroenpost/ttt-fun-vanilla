ITEM.Name = 'USP45'
ITEM.Price = 3500
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/usp45.png"
ITEM.WeaponClass = 'weapon_ttt_sipistol'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:CanPlayerBuy(ply)
    local timebetween = os.time() - (ply:GetPData( "lastweaponbuy"..self.WeaponClass, 1) * 1)
    if timebetween < 300 then
        ply:PS_Notify("You can only buy this weapon once every 5 minutes. Please wait "..(300-timebetween).." seconds")
        return false
    end

    return true

end


function ITEM:OnBuy(ply)

    ply:SetPData( "lastweaponbuy"..self.WeaponClass, os.time())
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
