CATEGORY.Name = 'Super Donator Weapons'
CATEGORY.Desc = 'Weapons for Super Donators. A super donator donated $50 or more.'
CATEGORY.Icon = 'user_suit'
CATEGORY.Order = 8
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "weapons"
CATEGORY.CanPlayerSee = function() return true end
function CATEGORY:CanPlayerBuy(ply) return gb.is_superdonator(ply) end