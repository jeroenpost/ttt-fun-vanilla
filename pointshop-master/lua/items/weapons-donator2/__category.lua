CATEGORY.Name = 'Donator+ Weapons'
CATEGORY.Desc = 'Weapons for Donator+. A donator+ donated $25 or more.'
CATEGORY.Icon = 'user_suit'
CATEGORY.Order = 7
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "weapons"
CATEGORY.CanPlayerSee = function() return true end
function CATEGORY:CanPlayerBuy(ply) return gb.is_donatorplus(ply) end