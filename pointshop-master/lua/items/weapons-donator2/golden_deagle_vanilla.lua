ITEM.Name = 'Perma Golden Deagle'
ITEM.Price = 250000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/golden_deagle.png"
ITEM.WeaponClass = 'gb_golden_deagle'
ITEM.SingleUse = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
