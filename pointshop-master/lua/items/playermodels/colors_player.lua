ITEM.Name = 'Color your model'
ITEM.Price = 25000
ITEM.Material = 'trails/rainbow'
 
function ITEM:OnEquip(ply, modifications)
	if not ply._OldColor then
		ply._OldColor = ply:GetPlayerColor()
	end
	if modifications.color ~= nil then
		borkolor = modifications.color
	ply:SetPlayerColor(Vector( borkolor.r / 255, borkolor.g / 255, borkolor.b / 255))
	end
end
 

function ITEM:OnHolster(ply)
	if ply._OldColor then
		ply:SetPlayerColor(ply._OldColor)
	end
end

function ITEM:Modify(modifications)
		PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
    self:OnHolster(ply)
    self:OnEquip(ply, modifications) -- adds the item back again, with new mods
end