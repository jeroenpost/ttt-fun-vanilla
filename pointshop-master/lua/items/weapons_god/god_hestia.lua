ITEM.Name = "Hestia's Kiss of Enlightment"
ITEM.Price = 50000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/hestias_kiss_of_enlightment.png"
ITEM.WeaponClass = 'weapon_gb_hestia'
ITEM.SingleUse = true

function ITEM:CanPlayerBuy(ply)
    local timebetween = os.time() - (ply:GetPData( "lastgodweaponbuy", 1) * 1)
    if timebetween < 900 then
        ply:PS_Notify("You can only buy a god weapon once every 15 minutes. Please wait "..(900-timebetween).." seconds")
        return false
    end

    return true

end


function ITEM:OnBuy(ply)

    ply:SetPData( "lastgodweaponbuy", os.time())
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 
