CATEGORY.Name = 'God Weapons'
CATEGORY.Desc = 'All the GOD weapons. Everyone can buy these. One hit kill.'
CATEGORY.Icon = 'user_suit'
CATEGORY.NotAllowedUserGroups = { "user","" }
CATEGORY.Order = 1
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "weapons"
CATEGORY.CanPlayerSee = function() return true end