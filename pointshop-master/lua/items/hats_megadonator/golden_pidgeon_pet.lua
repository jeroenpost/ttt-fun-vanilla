ITEM.Name = 'Golden Pigeon Pet'
ITEM.Price = 500000
ITEM.Model = 'models/pigeon.mdl'
ITEM.Skin = 'camos/camo7'
ITEM.Follower = 'golden_pigeon'

function ITEM:OnEquip(ply, modifications)
	ply:Fo_CreateFollower( self.Follower )
end

function ITEM:OnHolster(ply)
	ply:Fo_RemoveFollower( self.Follower )
end