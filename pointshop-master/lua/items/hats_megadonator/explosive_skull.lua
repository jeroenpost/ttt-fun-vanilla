ITEM.Name = 'Explosive Skully'
ITEM.Price = 500000
ITEM.Model = "models/weapons/w_eq_fraggrenade.mdl"
ITEM.Skin = 'camos/camo15'
ITEM.Follower = 'explosive_skull'

function ITEM:OnEquip(ply, modifications)
	ply:Fo_CreateFollower( self.Follower )
end

function ITEM:OnHolster(ply)
	ply:Fo_RemoveFollower( self.Follower )
end