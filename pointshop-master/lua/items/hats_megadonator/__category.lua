CATEGORY.Name = 'Pets & Hats - Megadonator'
CATEGORY.Icon = 'emoticon_smile'
CATEGORY.AllowedEquipped = 3
CATEGORY.Order = 1
CATEGORY.Desc = "Pets following you around!"
CATEGORY.BackgroundColor = Color( 150, 150, 150, 255 )
function CATEGORY:CanPlayerBuy(ply) return gb.is_megadonator(ply) end
