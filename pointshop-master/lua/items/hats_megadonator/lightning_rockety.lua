ITEM.Name = 'Lightning rocket'
ITEM.Price = 500000
ITEM.Model = 'models/Items/AR2_Grenade.mdl'
ITEM.Skin = 'camos/camo43'
ITEM.Follower = 'rocket_lightning'

function ITEM:OnEquip(ply, modifications)
	ply:Fo_CreateFollower( self.Follower )
end

function ITEM:OnHolster(ply)
	ply:Fo_RemoveFollower( self.Follower )
end