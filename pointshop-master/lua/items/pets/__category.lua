CATEGORY.Name = 'Pets & Hats'
CATEGORY.Desc = 'Pets for Super Donators'
CATEGORY.Icon = 'user_suit'

CATEGORY.Order = 8
CATEGORY.IsSubcategory = false

CATEGORY.CanPlayerSee = function() return true end

function CATEGORY:CanPlayerBuy(ply) return gb.is_superdonator(ply) end