ITEM.Name = 'MedKit'
ITEM.Price = 5500
ITEM.Model = "models/weapons/w_medkit.mdl"
ITEM.WeaponClass = 'gb_camo_medkit'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 
