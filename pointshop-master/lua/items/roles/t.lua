ITEM.Name = 'Traitor'
ITEM.Price = 15000
ITEM.Material = 'vgui/ttt/sprite_traitor.vmt'
ITEM.OneUse = true
ITEM.SingleUse = true
ITEM.buyWhenDeath = true

function ITEM:CanPlayerBuy(ply)

    local timebetween = os.time() - (ply:GetPData( "lasttraitorbuy", 1) * 1)
    if timebetween < 900 then
        ply:PS_Notify("You can only buy traitor once every 5 minutes. Please wait "..(900-timebetween).." seconds")
        return false
    end
  
     
      if GetGlobalInt("ttt_rounds_left", 6) < 4 then
       ply:PS_Notify("The next round will be special,you cannot buy traitornow")
	return false
     end


    return true
end

function ITEM:OnBuy(ply)
	--ply.GotTraitor = true;
    ply:SetPData( "IPDIforceTnr", 1)
    ply:SetPData( "lasttraitorbuy", os.time())
	print( ply:Nick().." bought Traitor")
	ply:PS_Notify("You will be traitor next round")
end

function ITEM:OnHolster(ply)
--hook.Remove("TTTBeginRound", ply:UniqueID() .. "_traitor")
end

function ITEM:OnSell(ply)
--hook.Remove("TTTBeginRound", ply:UniqueID() .. "_traitor")
end
