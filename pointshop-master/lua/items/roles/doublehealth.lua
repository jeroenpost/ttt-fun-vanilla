ITEM.Name = 'Double Health'
ITEM.Price = 5500
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/2xhealth.png'
ITEM.OneUse = true
ITEM.SingleUse = true
ITEM.buyWhenDeath = true

function ITEM:CanPlayerBuy(ply)

    local timebetween = os.time() - (ply:GetPData( "lastdoublebuy", 1) * 1)
    if timebetween < 900 then
        ply:PS_Notify("You can only buy double health once every 5 minutes. Please wait "..(900-timebetween).." seconds")
        return false
    end

    return true

end


function ITEM:OnBuy(ply)
    ply.GotDoubleHealth = true;
    ply:SetPData( "GotDoubleHealth", 2)
    ply:SetPData( "lastdoublebuy", os.time())
    print( ply:Nick().." bought Double Health")
    ply:PS_Notify("You spawn with double health next round")
    if SERVER then
        DamageLog("Health: " .. ply:Nick() .. " [" .. ply:GetRoleString() .. "] bought extra health next round")
    end
end

