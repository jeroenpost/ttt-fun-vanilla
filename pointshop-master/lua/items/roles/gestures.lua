ITEM.Name = 'Gestures (permanent)'
ITEM.Price = 50000
ITEM.Material = 'vgui/ttt/gestures.png'
ITEM.buyWhenDeath = true

function ITEM:OnEquip(ply)
	ply.canGesture = true	
end

function ITEM:OnBuy( ply )
	ply.canGesture = true
	ply:ConCommand("show_gestures")
end

function ITEM:OnHolster(ply)
	ply.canGesture = false
end

function ITEM:OnSell(ply)
	ply.canGesture = false
end
