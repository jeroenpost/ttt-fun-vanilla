CATEGORY.Name = 'Roles and Powerups'
CATEGORY.Desc = 'Be The Hidden, The Infected, a BOSS or just buy a T or D round'
CATEGORY.Icon = 'lightning'
CATEGORY.Order = 4
CATEGORY.id = "roles"
CATEGORY.CanPlayerSee = function() return true end
CATEGORY.buyWhenDeath = true