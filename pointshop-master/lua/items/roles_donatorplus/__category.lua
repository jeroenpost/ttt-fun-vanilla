CATEGORY.Name = 'Donator Plus Boosts/Powers'
CATEGORY.Desc = 'Donators Plus can buy these boosts and powers. Say !donate in chat to donate too!'
CATEGORY.Icon = 'key'
CATEGORY.AllowedUserGroups = gbscoreboard.donators_plus
CATEGORY.Order = 4
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "roles"
CATEGORY.CanPlayerSee = function() return true end
function CATEGORY:CanPlayerBuy(ply) return gb.is_donatorplus(ply) end