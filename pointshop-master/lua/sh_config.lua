PS.Config = {}

-- Edit below

PS.Config.DataProvider = 'json'

PS.Config.Branch = "https://raw.github.com/adamdburton/pointshop/master/" -- Master is most stable, used for version checking.
PS.Config.CheckVersion = false -- Do you want to be notified when a new version of Pointshop is avaliable?

PS.Config.ShopKey = "F3" -- F1, F2, F3 or F4, or blank to disable
PS.Config.ShopCommand = 'ps_shop' -- Console command to open the shop, set to blank to disable
PS.Config.ShopChatCommand = '!shop' -- Chat command to open the shop, set to blank to disable

PS.Config.PointsName = 'Points'

PS.Config.NotifyOnJoin = true -- Should players be notified about opening the shop when they spawn?

PS.Config.PointsOverTime = true -- Should players be given points over time?
PS.Config.PointsOverTimeDelay = 1 -- If so, how many minutes apart?
PS.Config.PointsOverTimeAmount = 50 -- And if so, how many points to give after the time?

PS.Config.AdminCanAccessAdminTab = false -- Can Admins access the Admin tab?
PS.Config.SuperAdminCanAccessAdminTab = true -- Can SuperAdmins access the Admin tab?

PS.Config.CanPlayersGivePoints = true -- Can players give points away to other players?
PS.Config.DisplayPreviewInMenu = true -- Can players see the preview of their items in the menu?

PS.Config.Categories = {
    [1] = { "weapons","weapons_god","weapons-member","weapons-donator", "weapons-donator2", "weapons-superdonator" },
    [2] = { "playermodels","playermodels-member","playermodels-donator" },
    [3] = { "roles","roles_member","roles_donator", "roles_donatorplus", "roles_superdonator", "powerups_megadonator" },
    [4] = { "pets", "hats_megadonator" },
}

-- Edit below if you know what you're doing

PS.Config.CalculateBuyPrice = function(ply, item)
-- You can do different calculations here to return how much an item should cost to buy.
-- There are a few examples below, uncomment them to use them.

    if( item.IsCustomDB)then
        return item.Price
    end

    if string.sub(item.Name,1,3) == "cm_" or string.sub(item.Name,1,5) == "upgr_" then
        if  PS:IsDonator( ply ) then
            return item.Price
        else
            return math.Round(item.Price*0.25)
        end
    end

    -- Everything half price for admins:

    if item.Name == "Cheater" then
        if ply.PS_Points > 1750000 then
            return 499999
        end
    end


    -- 25% off for the 'donators' groups
    if PS:IsDonator( ply )  then return math.Round(item.Price * 0.5) end

    return item.Price
end
function PS:IsDonator(ply)


    for k,v in pairs(gbscoreboard.donators) do
        if ply:IsUserGroup(k) or ply.baseGroup == k then return true end
    end


    return false

end

PS.Config.CalculateSellPrice = function(ply, item)

    local priceItem = PS.Config.CalculateBuyPrice( ply, item)

    if  PS:IsDonator( ply ) then
        return  math.Round((item.Price * 0.5) * 0.75) end


    return math.Round(priceItem * 0.75) -- 75% or 3/4 (rounded) of the original item price
end